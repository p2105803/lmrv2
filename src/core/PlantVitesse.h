#ifndef _PLANTVITESSE_H_
#define _PLANTVITESSE_H_

#include "Map.h"
#include "PlantVie.h"
#include <cstdlib>
#include <iostream>
#include <ctime>

/**
 * @class PlantVitesse
 *
 * @brief Représente une plante de vitesse dans le jeu.
 *
 * La classe PlantVitesse est utilisée pour représenter une plante de vitesse dans le jeu. Une plante de vitesse est un objet qui peut être activé pour augmenter la vitesse du joueur pendant une certaine période de temps.
 */
class PlantVitesse
{
private:
    int p_posX; ///< La position X de la plante de vitesse.
    int p_posY; ///< La position Y de la plante de vitesse.
    bool p_actif; ///< Indique si la plante de vitesse est active ou non.
    time_t usedTime; ///< Le temps d'utilisation de la plante de vitesse.

public:
    /**
     * @brief Constructeur de la classe PlantVitesse.
     *
     * @param map La carte du jeu.
     *
     * Le constructeur initialise les membres de la classe et génère une position aléatoire pour la plante de vitesse, en s'assurant qu'elle ne se trouve pas sur un terrain interdit.
     */
    PlantVitesse(const Map &map) : p_posX(0), p_posY(0), p_actif(false), usedTime(0)
    {
        do
        {
            p_posX = (rand() % map.getDimX()) / 24;
            p_posY = (rand() % map.getDimY()) / 24;
            p_actif = true;
        } while (map.getTerrainAt(p_posX, p_posY) == '#' || map.getTerrainAt(p_posX, p_posY) == '2');
    }

    /**
     * @brief Active ou désactive la plante de vitesse.
     *
     * @param actif Indique si la plante de vitesse doit être activée (true) ou désactivée (false).
     */
    void setActive(bool actif) { p_actif = actif; }

    /**
     * @brief Définit la position X de la plante de vitesse.
     *
     * @param posX La nouvelle position X de la plante de vitesse.
     */
    void setPosX(int posX);

    /**
     * @brief Définit la position Y de la plante de vitesse.
     *
     * @param posY La nouvelle position Y de la plante de vitesse.
     */
    void setPosY(int posY);

    /**
     * @brief Obtient la position X de la plante de vitesse.
     *
     * @return La position X de la plante de vitesse.
     */
    int getPosX() const;

    /**
     * @brief Obtient la position Y de la plante de vitesse.
     *
     * @return La position Y de la plante de vitesse.
     */
    int getPosY() const;

    /**
     * @brief Indique si la plante de vitesse est active ou non.
     *
     * @return true si la plante de vitesse est active, false sinon.
     */
    bool estActif() const;

    /**
     * @brief Définit le temps d'utilisation de la plante de vitesse.
     *
     * @param time Le temps d'utilisation de la plante de vitesse.
     */
    void setUsedTime(time_t time);

    /**
     * @brief Obtient le temps d'utilisation de la plante de vitesse.
     *
     * @return Le temps d'utilisation de la plante de vitesse.
     */
    time_t getUsedTime() const;
};

/**
 * @brief Définit la position X de la plante.
 * 
 * Cette fonction permet de définir la position X de la plante.
 * 
 * @param posX La nouvelle position X de la plante.
 */
inline void PlantVitesse::setPosX(int posX) { p_posX = posX; }
/**
 * @brief Définit la position Y de la plante.
 * 
 * Cette fonction permet de définir la position Y de la plante.
 * 
 * @param posY La position Y de la plante.
 */
inline void PlantVitesse::setPosY(int posY) { p_posY = posY; }
/**
 * @brief Obtient la position X de la plante.
 * 
 * @return La position X de la plante.
 */
inline int PlantVitesse::getPosX() const { return p_posX; }
/**
 * @brief Obtient la position Y de la plante.
 * 
 * @return La position Y de la plante.
 */
inline int PlantVitesse::getPosY() const { return p_posY; }
/**
 * @brief Vérifie si la plante de vitesse est active.
 * 
 * @return true si la plante de vitesse est active, sinon false.
 */
inline bool PlantVitesse::estActif() const { return p_actif; }
/**
 * @brief Renvoie le temps utilisé.
 *
 * Cette fonction renvoie le temps utilisé de l'objet PlantVitesse.
 *
 * @return Le temps utilisé.
 */
inline time_t PlantVitesse::getUsedTime() const { return usedTime; }
/**
 * @brief Définit le temps utilisé de la plante.
 * 
 * Cette fonction permet de définir le temps utilisé de la plante en spécifiant le temps en secondes.
 * 
 * @param time Le temps utilisé de la plante en secondes.
 */
inline void PlantVitesse::setUsedTime(time_t time) { usedTime = time; }

#endif