#include "Jeu.h"
#include "math.h"

Jeu::Jeu() : map(), joueur(22, 7, 50, 10.f, 0), plantVie(map), plantVitesse(map), nbMonstres(4), etatJeu(true)
{
    competence.resize(2);
    competence[0] = nullptr;
    competence[1] = nullptr;
    for (int i = 0; i < nbMonstres; i++)
    {
        monstres.push_back(Monstre(joueur, map));
    }
}

Jeu::~Jeu()
{
    for (size_t i = 0; i < competence.size(); i++)
    {
        if (competence[i] != nullptr)
        {
            delete competence[i];
            competence[i] = nullptr;
        }
    }
    monstres.clear();
}

void Jeu::deplacementJoueurSouris(const float posX, const float posY, float dt)
{
    joueur.deplacerSouris(posX, posY, map, monstres, dt, plantVie);
}

void Jeu::deplacementJoueur(const char direction, float dt)
{
    switch (direction)
    {
    case 'z':
        joueur.haut(map, dt);
        break;
    case 's':
        joueur.bas(map, dt);
        break;
    case 'q':
        joueur.gauche(map, dt);
        break;
    case 'd':
        joueur.droite(map, dt);
        break;
    default:
        break;
    }
}

void Jeu::utiliserCompetenceTir(const char c, float dt)
{
    switch (c)
    {
    case 'd':
    case 'q':
    case 's':
    case 'z':
        if (competence[1] == nullptr || !competence[1]->getActive())
        {
            delete competence[1];
            competence[1] = new TirMystique(joueur.getPosX(), joueur.getPosY(), true);
        }
        competence[1]->utiliser(map, joueur, monstres, c, dt);
        break;
    default:
        break;
    }
}

void Jeu::utiliserCompetenceFlash(const char c, float dt)
{
    switch (c)
    {
    case 'd':
    case 'q':
    case 's':
    case 'z':
        competence[0] = new Flash();
        competence[0]->utiliser(map, joueur, monstres, c, dt);
        break;
    default:
        break;
    }
}

void Jeu::utiliserCompetenceSouris(const char c, float posX, float posY, float dt)
{
    switch (c)
    {
    case 'f':
        if (competence[0] != nullptr)
        {
            delete competence[0];
            competence[0] = nullptr;
        }
        competence[0] = new Flash();
        competence[0]->utiliserSouris(map, joueur, monstres, posX, posY, dt);
        break;
    case 'g':
        if (competence[1] == nullptr)
        {
            competence[1] = new TirMystique(joueur.getPosX(), joueur.getPosY(), true);
        }
        competence[1]->utiliserSouris(map, joueur, monstres, posX, posY, dt);
        break;
    default:
        break;
    }
}

void Jeu::tuerMonstre()
{
    for (int i = 0; i < nbMonstres; i++)
    {
        if (monstres[i].getVie() == 0)
        {
            std::vector<Monstre>::iterator it = monstres.begin();
            advance(it, i);
            monstres.erase(it);
            nbMonstres = monstres.size();
        }
    }
    tempsDepuisMortMonstre = 0.f;
}

void Jeu::spawnMonstre(float dt)
{
    tempsDepuisMortMonstre += dt;
    if (etatJeu == true)
    {
        if (monstres.size() < 3 && tempsDepuisMortMonstre >= 3.0f)
        {
            monstres.push_back(Monstre(joueur, map));
            nbMonstres = monstres.size();
            tempsDepuisMortMonstre = 0.0f;
        }
    }
}

void Jeu::spawnVagueMonstre(float dt)
{
    tempsJeu += dt;
    if (tempsJeu >= 5.0f)
    {
        monstres.push_back(Monstre(joueur, map));
        nbMonstres = monstres.size();
        tempsJeu = 0.0f;
    }
}

void Jeu::updateSouris(float dt)
{
    if (etatJeu == true)
    {
        if (joueur.getVie() > 0)
        {
            for (int i = 0; i < nbMonstres; i++)
            {
                bool collision = false;
                for (int j = 0; j < nbMonstres; j++)
                {
                    if (i != j) // Ne pas vérifier le monstre avec lui-même
                    {
                        float dx = monstres[j].getPosX() - monstres[i].getPosX();
                        float dy = monstres[j].getPosY() - monstres[i].getPosY();
                        float distance = std::sqrt(dx * dx + dy * dy);
                        if (distance < 1) // Si la distance est inférieure à 1, il y a une collision
                        {
                            collision = true;
                            break;
                        }
                    }
                }

                if (collision)
                {
                    monstres[i].deplacerAleatoirementModeSouris(joueur, map, monstres, dt);
                }
                else
                {
                    // Si aucune collision n'est détectée, déplacez le monstre vers le joueur
                    float dx = joueur.getPosX() - monstres[i].getPosX();
                    float dy = joueur.getPosY() - monstres[i].getPosY();
                    float distance = std::sqrt(dx * dx + dy * dy);
                    if (distance > 1.2)
                    {
                        monstres[i].deplacerVersJoueurModeSouris(joueur, map, monstres, dt, i);
                    }
                }
            }

            for (int i = 0; i < nbMonstres; i++)
            {
                if ((int)std::sqrt(std::pow(joueur.getPosX() - monstres[i].getPosX(), 2) + std::pow(joueur.getPosY() - monstres[i].getPosY(), 2)) <= 1)
                {
                    joueur.setVie(joueur.getVie() - 2 * dt);
                }
            }
            if (competence[1] != nullptr && competence[1]->getMonstreTouche())
            {
                int i = competence[1]->getIndiceMonstreTouche();
                monstres[i].setVie(0);
                tuerMonstre();
            }

            if (competence[1] != nullptr && competence[1]->getActive() == false)
            {
                delete competence[1];
                competence[1] = nullptr;
            }
            if (joueur.getVie() <= 0)
            {
                etatJeu = false;
            }
        }
    }
}

void Jeu::updateClavier(float dt)
{
    if (etatJeu == true)
    {
        if (joueur.getVie() > 0)
        {
            for (int i = 0; i < nbMonstres; i++)
            {
                bool collision = false;
                for (int j = 0; j < nbMonstres; j++)
                {
                    if (i != j) // Ne pas vérifier le monstre avec lui-même
                    {
                        float dx = monstres[j].getPosX() - monstres[i].getPosX();
                        float dy = monstres[j].getPosY() - monstres[i].getPosY();
                        float distance = std::sqrt(dx * dx + dy * dy);
                        if (distance < 1) // Si la distance est inférieure à 1, il y a une collision
                        {
                            collision = true;
                            break;
                        }
                    }
                }

                if (collision)
                {
                    monstres[i].deplacerAleatoirementModeClavier(joueur, map, monstres);
                }
                else
                {
                    // Si aucune collision n'est détectée, déplacez le monstre vers le joueur
                    float dx = joueur.getPosX() - monstres[i].getPosX();
                    float dy = joueur.getPosY() - monstres[i].getPosY();
                    float distance = std::sqrt(dx * dx + dy * dy);
                    if (distance > 1.2)
                    {
                        monstres[i].deplacerVersJoueurModeClavier(joueur, map, monstres, dt, i);
                    }
                }
            }

            for (int i = 0; i < nbMonstres; i++)
            {
                if ((int)std::sqrt(std::pow(joueur.getPosX() - monstres[i].getPosX(), 2) + std::pow(joueur.getPosY() - monstres[i].getPosY(), 2)) <= 1)
                {
                    joueur.setVie(joueur.getVie() - 1);
                }
            }

            if (competence[1] != nullptr && competence[1]->getMonstreTouche())
            {
                int i = competence[1]->getIndiceMonstreTouche();
                monstres[i].setVie(0);
                tuerMonstre();
            }

            if (competence[1] != nullptr && competence[1]->getActive() == false)
            {
                delete competence[1];
                competence[1] = nullptr;
            }
            if (joueur.getVie() <= 0)
            {
                etatJeu = false;
            }
        }
    }
}

void Jeu::afficher() const
{
    if (etatJeu == false)
    {
        std::cout << "Perdu" << std::endl;
    }
    else
    {
        std::cout << "Vie : " << joueur.getVie() << std::endl;
        for (int i = 0; i < map.getDimY(); i++)
        {
            for (int j = 0; j < map.getDimX(); j++)
            {
                if (joueur.getPosX() == j && joueur.getPosY() == i)
                {
                    std::cout << "J";
                }
                else
                {
                    bool monstrePresent = false;
                    for (int k = 0; k < nbMonstres; k++)
                    {
                        if (monstres[k].getPosX() == j && monstres[k].getPosY() == i)
                        {
                            std::cout << "M";
                            monstrePresent = true;
                            break;
                        }
                    }
                    if (!monstrePresent)
                    {
                        std::cout << map.getTerrainAt(j, i);
                    }
                }
            }
            std::cout << std::endl;
        }
    }
}

void Jeu::clickJoueurSurPlanteVie(int clickPosX, int clickPosY)
{
    // Calculer la distance entre le joueur et la position du clic
    int distance = std::sqrt(std::pow(clickPosX - joueur.getPosX(), 2) + std::pow(clickPosY - joueur.getPosY(), 2));
    std::cout << " vieJ: " << joueur.getVie() << std::endl;
    // Vérifier si le joueur est à une distance de 3 ou moins de la plante
    if (distance <= 3 && clickPosX == plantVie.getPosX() && clickPosY == plantVie.getPosY())
    {
        // Augmenter la vie du joueur de 5
        joueur.setVie(joueur.getVie() + 5);
        plantVie.setActive(false);
        plantVie.setUsedTime(std::time(0));
    }
}

void Jeu::clickJoueurSurPlanteVitesse(int clickPosX, int clickPosY)
{
    // Calculer la distance entre le joueur et la position du clic
    int distance = std::sqrt(std::pow(clickPosX - joueur.getPosX(), 2) + std::pow(clickPosY - joueur.getPosY(), 2));
    std::cout << " VitesseJ: " << joueur.getVitesseJ() << std::endl;
    // Vérifier si le joueur est à une distance de 3 ou moins de la plante
    if (distance <= 3 && clickPosX == plantVitesse.getPosX() && clickPosY == plantVitesse.getPosY())
    {
        // Augmenter la vitesse du joueur de 5
        joueur.activateSpeedBoost();
        joueur.setVitesseJ(joueur.getVitesseJ());

        plantVitesse.setActive(false);
        plantVitesse.setUsedTime(std::time(0));
    }
}

void Jeu::joueurProchePlanteVie()
{
    // Calculer la distance entre le joueur et la plante de vie
    int distance = std::sqrt(std::pow(plantVie.getPosX() - joueur.getPosX(), 2) + std::pow(plantVie.getPosY() - joueur.getPosY(), 2));
    // std::cout << "Distance vie : " << distance << std::endl;
    //  Si la distance est inférieure ou égale à 3, activer la plante
    if (distance <= 1)
    {
        joueur.setVie(joueur.getVie() + 5);
        plantVie.setActive(false);
        plantVie.setUsedTime(std::time(0));
    }
}

void Jeu::joueurProchePlanteVitesse()
{
    // Calculer la distance entre le joueur et la plante de vitesse
    int distance = std::sqrt(std::pow(plantVitesse.getPosX() - joueur.getPosX(), 2) + std::pow(plantVitesse.getPosY() - joueur.getPosY(), 2));
    // std::cout << "Distance vitesse : " << distance << std::endl;
    //  Si la distance est inférieure ou égale à 3, activer la plante
    if (distance <= 1)
    {
        joueur.activateSpeedBoost();
        joueur.setVitesseJ(joueur.getVitesseJ());
        plantVitesse.setActive(false);
        plantVitesse.setUsedTime(std::time(0));
    }
}