#ifndef _MAP_H_
#define _MAP_H_

#include <cassert>
#include <vector>




/**
 * @class Map
 *
 * @brief Représente une carte du jeu.
 *
 * La classe Map représente une carte du jeu avec des cases de différents types.
 * Elle permet de manipuler et d'accéder aux informations de la carte.
 */
class Map
{
    private:
        /**
         * @enum TypeCase
         *
         * @brief Enumération des types de cases possibles.
         *
         * L'énumération TypeCase définit les différents types de cases possibles sur la carte.
         */
        enum TypeCase
        {
            MUR = '#',      ///< Case de type mur.
            VIDE = '+',     ///< Case de type vide.
            BUISSON = '2'   ///< Case de type buisson.
            //MONSTRE = 'M',
            //JOUEUR = '2'
        };

        int m_dimX; ///< Dimension X de la carte.
        int m_dimY; ///< Dimension Y de la carte.
        std::vector<std::vector<char>> m_terrain; ///< Matrice représentant la carte.

    public:
        /**
         * @brief Constructeur par défaut de la classe Map.
         *
         * Le constructeur par défaut initialise les dimensions de la carte à 0.
         */
        Map();

        /**
         * @brief Destructeur de la classe Map.
         *
         * Le destructeur de la classe Map ne fait rien de particulier.
         */
        ~Map();

        /**
         * @brief Vérifie si une position est valide sur la carte.
         *
         * @param x Coordonnée X de la position à vérifier.
         * @param y Coordonnée Y de la position à vérifier.
         * @return true si la position est valide, false sinon.
         */
        bool estPositionValide(float x, float y) const;

        /**
         * @brief Vérifie si une position est valide sur la carte en utilisant des coordonnées clavier.
         *
         * @param x Coordonnée X de la position à vérifier.
         * @param y Coordonnée Y de la position à vérifier.
         * @return true si la position est valide, false sinon.
         */
        bool estPositionValideClavier(int x, int y) const;

        /**
         * @brief Vérifie si une position est valide sur la carte en utilisant des coordonnées de la carte.
         *
         * @param x Coordonnée X de la position à vérifier.
         * @param y Coordonnée Y de la position à vérifier.
         * @return true si la position est valide, false sinon.
         */
        bool estPositionValideMap(int x, int y) const;

        /**
         * @brief Modifie le type de case à une position donnée sur la carte.
         *
         * @param x Coordonnée X de la position.
         * @param y Coordonnée Y de la position.
         * @param c Nouveau type de case.
         */
        void setTerrainAt(int x, int y, char c);

        /**
         * @brief Obtient le type de case à une position donnée sur la carte.
         *
         * @param x Coordonnée X de la position.
         * @param y Coordonnée Y de la position.
         * @return Type de case à la position donnée.
         */
        char getTerrainAt(int x, int y) const;

        /**
         * @brief Obtient la dimension X de la carte.
         *
         * @return Dimension X de la carte.
         */
        int getDimX() const;

        /**
         * @brief Obtient la dimension Y de la carte.
         *
         * @return Dimension Y de la carte.
         */
        int getDimY() const;
};

/**
 * Modifie le terrain à la position spécifiée.
 * 
 * @param x La coordonnée x de la position.
 * @param y La coordonnée y de la position.
 * @param c Le caractère représentant le terrain à placer.
 */
inline void Map::setTerrainAt(int x, int y, char c)
{
    //assert(estPositionValide(x, y));
    m_terrain[y][x] = c;
}

/**
 * @brief Obtient le terrain à la position spécifiée.
 * 
 * @param x La coordonnée x de la position.
 * @param y La coordonnée y de la position.
 * @return Le terrain à la position spécifiée.
 */
inline char Map::getTerrainAt(int x, int y) const { 
    assert(estPositionValideMap(x, y));
    return m_terrain[y][x]; 
}

/**
 * @brief Renvoie la dimension X de la carte.
 * 
 * @return La dimension X de la carte.
 */
inline int Map::getDimX() const { return m_dimX; }

/**
 * @brief Renvoie la dimension Y de la carte.
 * 
 * @return La dimension Y de la carte.
 */
inline int Map::getDimY() const { return m_dimY; }


#endif