#include "Flash.h"
#include <iostream>
#include <math.h>
#include "../Jeu.h"

Flash::Flash()
    : Competence()
{
    setActive(true);
}

void Flash::utiliser(Map &m, Joueur &j, const std::vector<Monstre> monstres, char c, float dt)
{
    if (!getActive())
    {
        return;
    }
    int dx = 0;
    int dy = 0;
    switch (c)
    {
    case 'd':
        dx = 1;
        break;
    case 'q':
        dx = -1;
        break;
    case 'z':
        dy = -1;
        break;
    case 's':
        dy = 1;
        break;
    default:
        break;
    }
    utiliserSouris(m, j, monstres, j.getPosX() + dx * distanceFlash, j.getPosY() + dy * distanceFlash, dt);
}

void Flash::utiliserSouris(Map &m, Joueur &j, const std::vector<Monstre> monstres, float posX, float posY, float dt)
{
    float dx = posX - j.getPosX();
    float dy = posY - j.getPosY();
    float distancePos = std::sqrt(dx * dx + dy * dy);
    float ratio;

    if (distancePos > distanceFlash)
    {
        ratio = distanceFlash / distancePos;
    }
    else
    {
        ratio = 1;
    }

    dx *= ratio;
    dy *= ratio;

    float newX = j.getPosX() + dx;
    float newY = j.getPosY() + dy;

    while (!m.estPositionValide(newX, newY))
    {
        newX -= dx * 0.1f;
        newY -= dy * 0.1f;
        if (m.estPositionValide(newX, newY))
        {
            break;
        }
    }

    j.setPosX(newX);
    j.setPosY(newY);
    setActive(false);
}