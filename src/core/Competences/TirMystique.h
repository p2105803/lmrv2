#ifndef _TIRMYSTIQUE_H
#define _TIRMYSTIQUE_H

#include "../Competence.h"

class TirMystique : public Competence {
    private:
        float vitesseC = 7.0f;
        unsigned int distanceTirMystique = 10;
        float distanceParcourue = 0;

public:
    TirMystique(float posX, float posY, bool Active);
    void utiliser(Map &m, Joueur &j, const std::vector<Monstre> monstres, char c, float dt) override;
    void utiliserSouris (Map &m, Joueur &j, const std::vector<Monstre> monstres, float posX, float posY, float dt) override;
};

#endif
