#ifndef _FLASH_H
#define _FLASH_H

#include "../Competence.h"
#include "../Joueur.h"

class Flash : public Competence {
private : 
    float distanceFlash = 4.f;

public:
    Flash();
    void utiliser(Map &m, Joueur &j, const std::vector<Monstre> monstres, char c, float dt) override;
    void utiliserSouris (Map &m, Joueur &j, const std::vector<Monstre> monstres, float posX, float posY, float dt) override;
};

#endif
