#include "TirMystique.h"
#include <unistd.h>
#include <math.h>
#include "../Jeu.h"

TirMystique::TirMystique(float posX, float posY, bool Active)
    : Competence(posX, posY, Active) {}

void TirMystique::utiliser(Map &m, Joueur &j, const std::vector<Monstre> monstres, char c, float dt)
{
    if (!getActive())
    {
        return;
    }
    float dx = 0;
    float dy = 0;
    switch (c)
    {
    case 'd':
        dx = 1;
        break;
    case 'q':
        dx = -1;
        break;
    case 'z':
        dy = -1;
        break;
    case 's':
        dy = 1;
        break;
    default:
        break;
    }
    utiliserSouris(m, j, monstres, getPosX() + dx * distanceTirMystique, getPosY() + dy * distanceTirMystique, dt);
}

void TirMystique::utiliserSouris(Map &m, Joueur &j, const std::vector<Monstre> monstres, float posX, float posY, float dt)
{
    float dx = posX - getPosX();
    float dy = posY - getPosY();
    float distanceTir = std::sqrt(dx * dx + dy * dy);
    dx /= distanceTir;
    dy /= distanceTir;
    // std::cout << "dx : " << dx << " dy : " << dy << std::endl;
    distanceParcourue += std::sqrt(dx * dx + dy * dy) * dt * vitesseC;
    float newX = getPosX() + dx * dt * vitesseC;
    float newY = getPosY() + dy * dt * vitesseC;

    if (distanceTir < 0.1)
    {
        setPosX(posX);
        setPosY(posY);
    }
    else
    {
        setPosX(newX);
        setPosY(newY);
    }
    for (size_t i = 0; i < monstres.size(); i++)
    {
        float dxMonstre = monstres[i].getPosX() - getPosX();
        float dyMonstre = monstres[i].getPosY() - getPosY();
        float distanceMonstre = std::sqrt(dxMonstre * dxMonstre + dyMonstre * dyMonstre);
        if (distanceMonstre <= sqrt(2))
        {
            setMonstreTouche(true);
            setIndiceMonstreTouche(i);
            break;
        }
    }
    if (!m.estPositionValide(newX, newY))
    {
        setActive(false);
    }
    else if (distanceParcourue >= distanceTirMystique)
    {
        setActive(false);
    }
    else if (getMonstreTouche())
    {
        setActive(false);
    }
}
