#include "Map.h"

#include <fstream>
#include <iostream>
#include <string>
#include <limits>

Map::Map() {
    std::string bordure;
    std::ifstream mapFile("map.txt");
    if (!mapFile) {
        std::cerr << "Failed to open map.txt" << std::endl;
        return;
    }
    
    if (mapFile >> m_dimX >> m_dimY) {
        m_terrain.resize(m_dimY, std::vector<char>(m_dimX)); 

        std::getline(mapFile, bordure); 

        for (int i = 0; i < m_dimY; ++i) {
            if (std::getline(mapFile, bordure)) {
                for (int j = 0; j < m_dimX && j < static_cast<int>(bordure.size()); ++j) {
                    if (estPositionValideMap(j, i)) {
                        m_terrain[i][j] = bordure[j];
                    } else {
                        std::cerr << "Position invalide sur j: " << j << ", i: " << i << std::endl;
                    }
                }
            }
        }
    } else {
        std::cerr << "Erreur de lecture des dimensions dans map.txt" << std::endl;
    }
    mapFile.close();
}

Map::~Map() {
    m_terrain.clear();
}

bool Map::estPositionValide(float x, float y) const {
   if (!((x >= 1) && (x <= m_dimX - 1) && (y >= 1) && (y <= m_dimY - 1))){
         return false;
   }
   if (!(m_terrain[y][x] != '#')){
        return false;
   }
   if (!(m_terrain[y + 1][x] != '#')){
        return false;
   }
   if (!(m_terrain[y][x + 1] != '#')){
        return false;
   }
   if (!(m_terrain[y + 1][x + 1] != '#')){
        return false;
   }

   return true;
}

bool Map::estPositionValideClavier(int x, int y) const {
    return ((x >= 0) && (x < m_dimX) && (y >= 0) && (y < m_dimY) && (m_terrain[y][x] != '#'));
}

bool Map::estPositionValideMap(int x, int y) const {
    return ((x >= 0) && (x < m_dimX) && (y >= 0) && (y < m_dimY));
}
