#ifndef _PLANTVIE_H_
#define _PLANTVIE_H_

#include "Map.h"
#include <cstdlib>
#include <iostream>
#include <ctime>

class Joueur;

/**
 * @class PlantVie
 *
 * @brief Représente une plante dans le jeu.
 *
 * La classe PlantVie est utilisée pour représenter une plante dans le jeu. Elle contient des informations sur la position de la plante, son état d'activité et le temps d'utilisation.
 */
class PlantVie
{
private:
    int p_posX; ///< La position X de la plante.
    int p_posY; ///< La position Y de la plante.
    bool p_actif; ///< Indique si la plante est active ou non.
    time_t usedTime; ///< Le temps d'utilisation de la plante.

public:
    /**
     * @brief Constructeur de la classe PlantVie.
     *
     * @param map La carte du jeu.
     *
     * Ce constructeur initialise les membres de la classe PlantVie en choisissant une position aléatoire pour la plante, en s'assurant qu'elle ne se trouve pas sur un terrain interdit.
     */
    PlantVie(const Map &map) : p_posX(0), p_posY(0), p_actif(false), usedTime(0)
    {
        do
        {
            p_posX = (rand() % map.getDimX()) / 24;
            p_posY = (rand() % map.getDimY()) / 24;
            p_actif = true;
        } while (map.getTerrainAt(p_posX, p_posY) == '#' || map.getTerrainAt(p_posX, p_posY) == '2');
    }

    /**
     * @brief Définit l'état d'activité de la plante.
     *
     * @param actif Indique si la plante est active ou non.
     */
    void setActive(bool actif) { p_actif = actif; }

    /**
     * @brief Définit la position X de la plante.
     *
     * @param posX La position X de la plante.
     */
    void setPosX(int posX);

    /**
     * @brief Définit la position Y de la plante.
     *
     * @param posY La position Y de la plante.
     */
    void setPosY(int posY);

    /**
     * @brief Obtient la position X de la plante.
     *
     * @return La position X de la plante.
     */
    int getPosX() const;

    /**
     * @brief Obtient la position Y de la plante.
     *
     * @return La position Y de la plante.
     */
    int getPosY() const;

    /**
     * @brief Vérifie si la plante est active.
     *
     * @return True si la plante est active, False sinon.
     */
    bool estActif() const;

    /**
     * @brief Définit le temps d'utilisation de la plante.
     *
     * @param time Le temps d'utilisation de la plante.
     */
    void setUsedTime(time_t time);

    /**
     * @brief Obtient le temps d'utilisation de la plante.
     *
     * @return Le temps d'utilisation de la plante.
     */
    time_t getUsedTime() const;
};

/**
 * @brief Définit la position Y de la plante.
 * 
 * Cette fonction permet de définir la position Y de la plante dans l'environnement.
 * 
 * @param posY La position Y de la plante.
 */
inline void PlantVie::setPosY(int posY) { p_posY = posY; }
/**
 * @brief Obtient la position X de la plante.
 * 
 * @return La position X de la plante.
 */
inline int PlantVie::getPosX() const { return p_posX; }
/**
 * @brief Obtient la position Y de la plante.
 * 
 * @return La position Y de la plante.
 */
inline int PlantVie::getPosY() const { return p_posY; }

/**
 * @brief Vérifie si la plante est active.
 * 
 * @return true si la plante est active, false sinon.
 */
inline bool PlantVie::estActif() const { return p_actif; }
/**
 * @brief Renvoie le temps utilisé.
 * 
 * Cette fonction renvoie le temps utilisé par l'objet PlantVie.
 * 
 * @return Le temps utilisé.
 */
inline time_t PlantVie::getUsedTime() const { return usedTime; }
/**
 * @brief Définit le temps d'utilisation de la plante.
 * 
 * Cette fonction permet de définir le temps d'utilisation de la plante en spécifiant un temps donné.
 * 
 * @param time Le temps d'utilisation de la plante.
 */
inline void PlantVie::setUsedTime(time_t time) { usedTime = time; }

#endif