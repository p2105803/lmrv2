#include "Monstre.h"
#include "../extern/Point.h"
#include "../extern/CalculTrajectoire.h"

#include <cmath>

Monstre::Monstre() : m_vie(1), m_posX(1.f), m_posY(1.f), destinationX(0), destinationY(0){
}

Point calculPosition(const Joueur &J, const Map &map)
{
    Point posJoueur;
    posJoueur.x = J.getPosX();
    posJoueur.y = J.getPosY();
    int zoneSafe = 3;
    Point tailleTerrain;
    tailleTerrain.x = map.getDimX();
    tailleTerrain.y = map.getDimY();

    Point positionMonstre;

    do
    {
        positionMonstre.x = static_cast<int>(rand() % static_cast<int>((tailleTerrain.x - 1) + 1)) / 24.0f;
        positionMonstre.y = static_cast<int>(rand() % static_cast<int>((tailleTerrain.y - 1) + 1)) / 24.0f;

        int distance = sqrt(pow(positionMonstre.x - posJoueur.x, 2) + pow(positionMonstre.y - posJoueur.y, 2));

        if (distance > zoneSafe && map.estPositionValide(positionMonstre.x, positionMonstre.y))
        {
            break;
        }
    } while (true);

    return positionMonstre;
}

Monstre::Monstre(const Joueur &j, const Map &m)
{
    m_vie = 1;
    Point pos = calculPosition(j, m);
    m_posX = (int)pos.x;
    m_posY = (int)pos.y;
    destinationX = 0;
    destinationY = 0;
    changerDestination(m);
}
void Monstre::changerDestination(const Map &m)
{
    do
    {
        destinationX = (rand() % (m.getDimX() - 1) + 1) / 16.0f;
        destinationY = (rand() % (m.getDimY() - 1) + 1) / 16.0f;
    } while (!m.estPositionValide(destinationX, destinationY) || m.getTerrainAt(destinationX, destinationY) == '2');
}

void Monstre::deplacerVersDestination(const Map &m, float dt)
{
    float dx = destinationX - m_posX;
    float dy = destinationY - m_posY;

    float distance = std::sqrt(dx * dx + dy * dy);

    dx /= distance;
    dy /= distance;

    m_posX += dx * vitesseM * dt;
    m_posY += dy * vitesseM * dt;
}

void Monstre::deplacerAleatoirementModeSouris(const Joueur &j, const Map &m, const std::vector<Monstre> &monstres, float dt)
{

    float dx = destinationX - m_posX;
    float dy = destinationY - m_posY;
    float distance = std::sqrt(dx * dx + dy * dy);

    if (distance < 0.1 || !m.estPositionValide(m_posX, m_posY))
    {
        changerDestination(m);
    }

    deplacerVersDestination(m, dt);
}

void Monstre::deplacerVersJoueurModeSouris(const Joueur &j, const Map &m, const std::vector<Monstre> &monstres, float dt, int indiceMonstre)
{
    Point joueurPos = {j.getPosX(), j.getPosY()};
    Point monstrePos = {m_posX, m_posY};
    CalculTrajectoire trajectoire;
    trajectoire.deplacerVersModeSouris(monstrePos, joueurPos, m, monstres, dt, vitesseM, indiceMonstre);
    if (monstrePos.x == m_posX && monstrePos.y == m_posY)
    {
        deplacerAleatoirementModeSouris(j, m, monstres, dt);
    }
    else
    {
        bool mouvementValide = true;
        for (size_t i = 0; i < monstres.size(); i++)
        {
            if (monstrePos.x == monstres[i].getPosX() && monstrePos.y == monstres[i].getPosY())
            {
                mouvementValide = false;
                break;
            }
        }

        if (mouvementValide && !(monstrePos.x == joueurPos.x && monstrePos.y == joueurPos.y) && m.estPositionValide(monstrePos.x, monstrePos.y))
        {
            m_posX = monstrePos.x;
            m_posY = monstrePos.y;
        }
    }
}

void Monstre::deplacerAleatoirementModeClavier(const Joueur &j, const Map &m, const std::vector<Monstre> &monstres)
{
    bool mouvementValide;
    do
    {
        mouvementValide = true;
        int direction = rand() % 4;
        int newX = m_posX;
        int newY = m_posY;

        switch (direction)
        {
        case 0:
            newX++;
            break;
        case 1:
            newX--;
            break;
        case 2:
            newY++;
            break;
        case 3:
            newY--;
            break;
        }

        if (m.estPositionValideClavier(newX, newY) && !(newX == j.getPosX() && newY == j.getPosY()))
        {
            for (size_t i = 0; i < monstres.size(); i++)
            {
                if (newX == monstres[i].getPosX() && newY == monstres[i].getPosY())
                {
                    mouvementValide = false;
                    break;
                }
            }
            if (mouvementValide)
            {
                m_posX = newX;
                m_posY = newY;
            }
        }
        else
        {
            mouvementValide = false;
        }
    } while (!mouvementValide);
}

void Monstre::deplacerVersJoueurModeClavier(const Joueur &j, const Map &m, const std::vector<Monstre> &monstres, float dt, int indiceMonstre)
{
    Point joueurPos = {j.getPosX(), j.getPosY()};
    Point monstrePos = {m_posX, m_posY};
    CalculTrajectoire trajectoire;
    trajectoire.deplacerVersModeClavier(monstrePos, joueurPos, m, monstres, dt, vitesseM, indiceMonstre);
    if (monstrePos.x == m_posX && monstrePos.y == m_posY)
    {
        deplacerAleatoirementModeClavier(j, m, monstres);
    }
    else
    {
        bool mouvementValide = true;
        for (size_t i = 0; i < monstres.size(); i++)
        {
            if (monstrePos.x == monstres[i].getPosX() && monstrePos.y == monstres[i].getPosY())
            {
                mouvementValide = false;
                break;
            }
        }

        if (mouvementValide && !(monstrePos.x == joueurPos.x && monstrePos.y == joueurPos.y) && m.estPositionValideClavier(monstrePos.x, monstrePos.y))
        {
            m_posX = monstrePos.x;
            m_posY = monstrePos.y;
        }
    }
}

bool Monstre::estVivant() const
{
    return m_vie > 0;
}
