#ifndef _JOUEUR_H
#define _JOUEUR_H

#include "Map.h"
#include <math.h>
#include "PlantVie.h"
#include <ctime>

class Monstre;
class PlantVie;

/**
 * @class Joueur
 * @brief Représente un joueur dans le jeu.
 * 
 * Cette classe contient les informations et les fonctionnalités liées à un joueur.
 */
class Joueur
{
private:
    float j_posX, j_posY; /**< La position en X et en Y du joueur. */
    float j_vie; /**< La vie du joueur. */
    float vitesseJ; /**< La vitesse de déplacement du joueur. */
    float boostVitesse; /**< La vitesse de déplacement du joueur lorsqu'il est en mode boost. */

    std::time_t boostEndTime = 0; /**< Le temps de fin du mode boost du joueur. */

public:
    /**
     * @brief Constructeur de la classe Joueur.
     * @param posX La position en X du joueur.
     * @param posY La position en Y du joueur.
     * @param vie La vie du joueur.
     * @param vitesse La vitesse de déplacement du joueur.
     * @param boostVitesse La vitesse de déplacement du joueur lorsqu'il est en mode boost.
     */
    Joueur(float posX, float posY, float vie, float vitesse, float boostVitesse);

    /**
     * @brief Constructeur par défaut de la classe Joueur.
     */
    Joueur();

    /**
     * @brief Déplace le joueur en fonction de la position de la souris.
     * @param posX La position en X de la souris.
     * @param posY La position en Y de la souris.
     * @param m La carte du jeu.
     * @param monstres Le vecteur contenant les monstres du jeu.
     * @param dt Le temps écoulé depuis la dernière mise à jour.
     * @param planteVie La plante de vie du jeu.
     */
    void deplacerSouris(float posX, float posY, const Map &m, const std::vector<Monstre> &monstres, float dt, const PlantVie &planteVie);

    /**
     * @brief Déplace le joueur en fonction des coordonnées spécifiées.
     * @param posX La position en X du joueur.
     * @param posY La position en Y du joueur.
     * @param m La carte du jeu.
     */
    void deplacer(float posX, float posY, const Map &m);

    /**
     * @brief Déplace le joueur vers la gauche.
     * @param m La carte du jeu.
     * @param dt Le temps écoulé depuis la dernière mise à jour.
     */
    void gauche(const Map &m, float dt);

    /**
     * @brief Déplace le joueur vers la droite.
     * @param m La carte du jeu.
     * @param dt Le temps écoulé depuis la dernière mise à jour.
     */
    void droite(const Map &m, float dt);

    /**
     * @brief Déplace le joueur vers le haut.
     * @param m La carte du jeu.
     * @param dt Le temps écoulé depuis la dernière mise à jour.
     */
    void haut(const Map &m, float dt);

    /**
     * @brief Déplace le joueur vers le bas.
     * @param m La carte du jeu.
     * @param dt Le temps écoulé depuis la dernière mise à jour.
     */
    void bas(const Map &m, float dt);

    /**
     * @brief Définit la vie du joueur.
     * @param vie La vie du joueur.
     */
    void setVie(float vie);

    /**
     * @brief Définit la position en X du joueur.
     * @param posX La position en X du joueur.
     */
    void setPosX(float posX);

    /**
     * @brief Définit la position en Y du joueur.
     * @param posY La position en Y du joueur.
     */
    void setPosY(float posY);

    /**
     * @brief Définit la vitesse de déplacement du joueur.
     * @param vitesseJ La vitesse de déplacement du joueur.
     */
    void setVitesseJ(float vitesseJ);

    /**
     * @brief Active le mode boost de vitesse du joueur.
     */
    void activateSpeedBoost();

    /**
     * @brief Retourne la vitesse de déplacement du joueur.
     * @return La vitesse de déplacement du joueur.
     */
    float getVitesseJ() const;

    /**
     * @brief Retourne la position en X du joueur.
     * @return La position en X du joueur.
     */
    float getPosX() const;

    /**
     * @brief Retourne la position en Y du joueur.
     * @return La position en Y du joueur.
     */
    float getPosY() const;

    /**
     * @brief Retourne la vie du joueur.
     * @return La vie du joueur.
     */
    float getVie() const;
};


/**
 * @brief Définit la valeur de la vie du joueur.
 * 
 * @param vie La nouvelle valeur de la vie.
 */
inline void Joueur::setVie(float vie) { j_vie = vie; }

/**
 * @brief Définit la position en X du joueur.
 * 
 * @param posX La nouvelle position en X du joueur.
 */
inline void Joueur::setPosX(float posX) { j_posX = posX; } // Changed to float

/**
 * @brief Définit la position Y du joueur.
 * 
 * Cette fonction permet de définir la position Y du joueur en spécifiant une valeur de type float.
 * 
 * @param posY La nouvelle position Y du joueur.
 */
inline void Joueur::setPosY(float posY) { j_posY = posY; } // Changed to float

/**
 * @brief Renvoie la position en X du joueur.
 * 
 * @return La position en X du joueur.
 */
inline float Joueur::getPosX() const { return j_posX; } // Changed to float

/**
 * @brief Renvoie la position Y du joueur.
 * 
 * @return La position Y du joueur.
 */
inline float Joueur::getPosY() const { return j_posY; } // Changed to float

/**
 * @brief Obtient la valeur de la vie du joueur.
 * 
 * @return La valeur de la vie du joueur.
 */
inline float Joueur::getVie() const { return j_vie; }

/**
 * Définit la vitesse du joueur.
 * 
 * @param vitJ La vitesse du joueur.
 */
inline void Joueur::setVitesseJ(float vitJ) { vitesseJ = vitJ; }

/**
 * @brief Renvoie la vitesse du joueur.
 *
 * Cette fonction renvoie la vitesse actuelle du joueur.
 *
 * @return La vitesse du joueur.
 */
inline float Joueur::getVitesseJ() const
{
    return vitesseJ;
}

#endif
