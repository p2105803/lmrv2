#ifndef _COMPETENCE_H
#define _COMPETENCE_H

#include "Map.h"
#include "Joueur.h"
#include "Monstre.h"

class Jeu;

/**
 * @class Competence
 *
 * @brief Classe représentant une compétence dans le jeu.
 *
 * Cette classe permet de définir et de gérer les compétences des joueurs dans le jeu.
 */
class Competence
{
private:
    float c_posX;                 ///< Position X de la compétence.
    float c_posY;                 ///< Position Y de la compétence.
    bool isActive = false;        ///< Indique si la compétence est active.
    bool monstreTouche = false;   ///< Indique si un monstre a été touché par la compétence.
    int indiceMonstreTouche = -1; ///< Indice du monstre touché par la compétence.

public:
    /**
     * @brief Constructeur par défaut de la classe Competence.
     */
    Competence() : c_posX(-1.0f), c_posY(-1.0f){};

    /**
     * @brief Constructeur de la classe Competence.
     *
     * @param posX Position X de la compétence.
     * @param posY Position Y de la compétence.
     * @param Active Indique si la compétence est active.
     */
    Competence(float posX, float posY, bool Active) : c_posX(posX), c_posY(posY), isActive(Active){};

    /**
     * @brief Destructeur virtuel de la classe Competence.
     */
    virtual ~Competence() = default;

    /**
     * @brief Utilise la compétence sur une carte, un joueur et des monstres.
     *
     * @param m La carte sur laquelle la compétence est utilisée.
     * @param j Le joueur qui utilise la compétence.
     * @param monstres Les monstres présents sur la carte.
     * @param c Un caractère spécifique à la compétence.
     * @param dt Le temps écoulé depuis la dernière utilisation de la compétence.
     */
    virtual void utiliser(Map &m, Joueur &j, const std::vector<Monstre> monstres, char c, float dt) = 0;

    /**
     * @brief Utilise la compétence sur une carte, un joueur et des monstres en utilisant la souris.
     *
     * @param m La carte sur laquelle la compétence est utilisée.
     * @param j Le joueur qui utilise la compétence.
     * @param monstres Les monstres présents sur la carte.
     * @param posX Position X de la souris.
     * @param posY Position Y de la souris.
     * @param dt Le temps écoulé depuis la dernière utilisation de la compétence.
     */
    virtual void utiliserSouris(Map &m, Joueur &j, const std::vector<Monstre> monstres, float posX, float posY, float dt) = 0;

    /**
     * @brief Définit la position X de la compétence.
     *
     * @param posX La position X de la compétence.
     */
    void setPosX(float posX);

    /**
     * @brief Définit la position Y de la compétence.
     *
     * @param posY La position Y de la compétence.
     */
    void setPosY(float posY);

    /**
     * @brief Définit si la compétence est active ou non.
     *
     * @param Active Indique si la compétence est active.
     */
    void setActive(bool Active) { isActive = Active; }
    /**
     * @brief Définit si un monstre a été touché par la compétence.
     *
     * @param touche Indique si un monstre a été touché par la compétence.
     */
    void setMonstreTouche(bool touche) { monstreTouche = touche; }
    /**
     * @brief Définit l'indice du monstre touché par la compétence.
     *
     * @param indice L'indice du monstre touché par la compétence.
     */
    void setIndiceMonstreTouche(int indice) { indiceMonstreTouche = indice; }
    /**
     * @brief Obtient la position X de la compétence.
     *
     * @return La position X de la compétence.
     */
    float getPosX() const;

    /**
     * @brief Obtient la position Y de la compétence.
     *
     * @return La position Y de la compétence.
     */
    float getPosY() const;

    /**
     * @brief Obtient si la compétence est active ou non.
     *
     * @return Indique si la compétence est active.
     */
    bool getActive() const { return isActive; }
    /**
     * @brief Obtient si un monstre a été touché par la compétence.
     *
     * @return Indique si un monstre a été touché par la compétence.
     */
    bool getMonstreTouche() const { return monstreTouche; }
    /**
     * @brief Obtient l'indice du monstre touché par la compétence.
     *
     * @return L'indice du monstre touché par la compétence.
     */
    int getIndiceMonstreTouche() const { return indiceMonstreTouche; }
};

/**
 * @brief Définit la position X de la compétence.
 *
 * @param posX La position X de la compétence.
 */
inline void Competence::setPosX(float posX) { c_posX = posX; }

/**
 * @brief Définit la position Y de la compétence.
 *
 * @param posY La position Y de la compétence.
 */
inline void Competence::setPosY(float posY) { c_posY = posY; }

/**
 * @brief Obtient la position X de la compétence.
 *
 * @return La position X de la compétence.
 */
inline float Competence::getPosX() const { return c_posX; }

/**
 * @brief Obtient la position Y de la compétence.
 *
 * @return La position Y de la compétence.
 */
inline float Competence::getPosY() const { return c_posY; }

#endif // _COMPETENCE_H