#ifndef _MONSTRE_H_
#define _MONSTRE_H_

#include "Joueur.h"
#include "Map.h"
#include <vector>

/**
 * @class Monstre
 *
 * @brief Représente un monstre dans le jeu.
 *
 * Cette classe gère les caractéristiques et les actions d'un monstre dans le jeu.
 */
class Monstre
{
private:
    int m_vie; ///< Le nombre de points de vie du monstre.
    float m_posX; ///< La position en X du monstre.
    float m_posY; ///< La position en Y du monstre.
    float destinationX; ///< La destination en X du monstre.
    float destinationY; ///< La destination en Y du monstre.
    float vitesseM = 5.0f; ///< La vitesse de déplacement du monstre.

public:
    /**
     * @brief Constructeur par défaut de la classe Monstre.
     */
    Monstre();

    /**
     * @brief Constructeur de la classe Monstre.
     * 
     * @param j Le joueur.
     * @param m La carte du jeu.
     */
    Monstre(const Joueur &j, const Map &m);

    /**
     * @brief Change la destination du monstre en fonction de la carte.
     * 
     * @param m La carte du jeu.
     */
    void changerDestination(const Map &m);

    /**
     * @brief Déplace le monstre vers sa destination en fonction de la carte et du temps écoulé.
     * 
     * @param m La carte du jeu.
     * @param dt Le temps écoulé depuis la dernière mise à jour.
     */
    void deplacerVersDestination(const Map &m, float dt);

    /**
     * @brief Déplace le monstre aléatoirement en mode souris.
     * 
     * @param j Le joueur.
     * @param m La carte du jeu.
     * @param monstres Le vecteur de monstres.
     * @param dt Le temps écoulé depuis la dernière mise à jour.
     */
    void deplacerAleatoirementModeSouris(const Joueur &j, const Map &m, const std::vector<Monstre> &monstres, float dt);

    /**
     * @brief Déplace le monstre vers le joueur en mode souris.
     * 
     * @param j Le joueur.
     * @param m La carte du jeu.
     * @param monstres Le vecteur de monstres.
     * @param dt Le temps écoulé depuis la dernière mise à jour.
     * @param indiceMonstre L'indice du monstre dans le vecteur.
     */
    void deplacerVersJoueurModeSouris(const Joueur &j, const Map &m, const std::vector<Monstre> &monstres, float dt, int indiceMonstre);

    /**
     * @brief Déplace le monstre aléatoirement en mode clavier.
     * 
     * @param j Le joueur.
     * @param m La carte du jeu.
     * @param monstres Le vecteur de monstres.
     */
    void deplacerAleatoirementModeClavier(const Joueur &j, const Map &m, const std::vector<Monstre> &monstres);

    /**
     * @brief Déplace le monstre vers le joueur en mode clavier.
     * 
     * @param j Le joueur.
     * @param m La carte du jeu.
     * @param monstres Le vecteur de monstres.
     * @param dt Le temps écoulé depuis la dernière mise à jour.
     * @param indiceMonstre L'indice du monstre dans le vecteur.
     */
    void deplacerVersJoueurModeClavier(const Joueur &j, const Map &m, const std::vector<Monstre> &monstres, float dt, int indiceMonstre);

    /**
     * @brief Vérifie si le monstre est vivant.
     * 
     * @return true si le monstre est vivant, false sinon.
     */
    bool estVivant() const;

    /**
     * @brief Définit le nombre de points de vie du monstre.
     * 
     * @param vie Le nombre de points de vie.
     */
    void setVie(int vie);

    /**
     * @brief Définit la position en X du monstre.
     * 
     * @param posX La position en X.
     */
    void setPosX(float posX);

    /**
     * @brief Définit la position en Y du monstre.
     * 
     * @param posY La position en Y.
     */
    void setPosY(float posY);

    /**
     * @brief Obtient le nombre de points de vie du monstre.
     * 
     * @return Le nombre de points de vie.
     */
    int getVie() const;

    /**
     * @brief Obtient la position en X du monstre.
     * 
     * @return La position en X.
     */
    float getPosX() const;

    /**
     * @brief Obtient la position en Y du monstre.
     * 
     * @return La position en Y.
     */
    float getPosY() const;
};

/**
 * @brief Définit la valeur de la vie du monstre.
 * 
 * Cette fonction permet de définir la valeur de la vie du monstre en lui attribuant
 * une nouvelle valeur spécifiée en paramètre.
 * 
 * @param vie La nouvelle valeur de la vie du monstre.
 */
inline void Monstre::setVie(int vie) { m_vie = vie; }
/**
 * @brief Définit la position en X du monstre.
 * 
 * Cette fonction permet de définir la position en X du monstre.
 * 
 * @param posX La nouvelle position en X du monstre.
 */
inline void Monstre::setPosX(float posX) { m_posX = posX; }
/**
 * @brief Définit la position Y du monstre.
 * 
 * Cette fonction permet de définir la position Y du monstre en spécifiant une valeur de type float.
 * 
 * @param posY La nouvelle position Y du monstre.
 */
/**
 * @brief Définit la position Y du monstre.
 * 
 * Cette fonction permet de définir la position Y du monstre.
 * 
 * @param posY La position Y du monstre.
 */
inline void Monstre::setPosY(float posY) { m_posY = posY; } 
/**
 * @brief Obtient la valeur de la vie du monstre.
 * 
 * @return La valeur de la vie du monstre.
 */
inline int Monstre::getVie() const { return m_vie; }
/**
 * @brief Obtient la position en X du monstre.
 * 
 * @return La position en X du monstre.
 */
inline float Monstre::getPosX() const { return m_posX; } 
/**
 * @brief Obtient la position Y du monstre.
 * 
 * @return La position Y du monstre.
 */
inline float Monstre::getPosY() const { return m_posY; } 

#endif
