#ifndef _JEU_H
#define _JEU_H

#include "Map.h"
#include "Joueur.h"
#include "Monstre.h"
#include "Competence.h"
#include "Competences/Flash.h"
#include "Competences/TirMystique.h"
#include "PlantVie.h"
#include "PlantVitesse.h" 
#include <vector>
#include <iostream>
#include <ctime>



class Map;
class Joueur;
class Monstre;
class Competence;
class Flash;
class TirMystique;
class PlantVie;
class PlantVitesse;

/**
 * @class Jeu
 *
 * @brief Classe représentant le jeu.
 *
 * Cette classe gère le déroulement du jeu, les interactions entre les différents éléments du jeu, 
 * et contient les méthodes pour effectuer les actions du joueur.
 */
class Jeu
{
    private:
        Map map; ///< La carte du jeu.
        Joueur joueur; ///< Le joueur.
        std::vector<Monstre> monstres; ///< Les monstres présents dans le jeu.
        std::vector<Competence*> competence; ///< Les compétences du joueur.
        PlantVie plantVie; ///< La plante de vie.
        PlantVitesse plantVitesse; ///< La plante de vitesse.
        int nbMonstres; ///< Le nombre de monstres présents dans le jeu.
        bool etatJeu; ///< L'état du jeu.

        float tirMystiqueCooldown = 3.f; ///< Le temps de recharge de la compétence de tir mystique.
        float flashCooldown = 5.f; ///< Le temps de recharge de la compétence de flash.

        float tempsDepuisMortMonstre = 0.f; ///< Le temps écoulé depuis la mort du dernier monstre.
        float tempsJeu = 0.f; ///< Le temps écoulé depuis le début du jeu.

    public:
        /**
         * @brief Constructeur par défaut de la classe Jeu.
         */
        Jeu();

        /**
         * @brief Destructeur de la classe Jeu.
         */
        ~Jeu();

        /**
         * @brief Obtient la carte du jeu.
         * 
         * @return La carte du jeu.
         */
        const Map& getMap() const;

        /**
         * @brief Obtient le joueur.
         * 
         * @return Le joueur.
         */
        const Joueur& getJoueur() const;

        /**
         * @brief Obtient les monstres présents dans le jeu.
         * 
         * @return Les monstres présents dans le jeu.
         */
        const std::vector<Monstre>& getMonstres() const;

        /**
         * @brief Obtient une compétence du joueur.
         * 
         * @param indice L'indice de la compétence.
         * @return La compétence correspondante.
         */
        const Competence* getCompetence(int indice) const;

        /**
         * @brief Obtient la plante de vie.
         * 
         * @return La plante de vie.
         */
        const PlantVie& getPlantVie() const;

        /**
         * @brief Obtient la plante de vitesse.
         * 
         * @return La plante de vitesse.
         */
        const PlantVitesse& getPlantVitesse() const;

        /**
         * @brief Utilise la compétence de tir.
         * 
         * @param c La touche correspondante à la compétence.
         * @param dt Le temps écoulé depuis la dernière frame.
         */
        void utiliserCompetenceTir(const char c, float dt);

        /**
         * @brief Utilise la compétence de flash.
         * 
         * @param c La touche correspondante à la compétence.
         * @param dt Le temps écoulé depuis la dernière frame.
         */
        void utiliserCompetenceFlash(const char c, float dt);

        /**
         * @brief Utilise la compétence de la souris.
         * 
         * @param c La touche correspondante à la compétence.
         * @param posX La position en X de la souris.
         * @param posY La position en Y de la souris.
         * @param dt Le temps écoulé depuis la dernière frame.
         */
        void utiliserCompetenceSouris(const char c, float posX, float posY, float dt);

        /**
         * @brief Déplace le joueur en fonction de la position de la souris.
         * 
         * @param posX La position en X de la souris.
         * @param posY La position en Y de la souris.
         * @param dt Le temps écoulé depuis la dernière frame.
         */
        void deplacementJoueurSouris(const float posX, const float posY, float dt);

        /**
         * @brief Déplace le joueur dans une direction donnée.
         * 
         * @param direction La direction du déplacement.
         * @param dt Le temps écoulé depuis la dernière frame.
         */
        void deplacementJoueur(const char direction, float dt);

        /**
         * @brief Tue le monstre le plus proche du joueur.
         */
        void tuerMonstre();

        /**
         * @brief Gère le clic du joueur sur la plante de vie.
         * 
         * @param clickPosX La position en X du clic.
         * @param clickPosY La position en Y du clic.
         */
        void clickJoueurSurPlanteVie(int clickPosX, int clickPosY);

        /**
         * @brief Gère le clic du joueur sur la plante de vitesse.
         * 
         * @param clickPosX La position en X du clic.
         * @param clickPosY La position en Y du clic.
         */
        void clickJoueurSurPlanteVitesse(int clickPosX, int clickPosY);

        /**
         * @brief Vérifie si le joueur est proche de la plante de vie.
         */
        void joueurProchePlanteVie();

        /**
         * @brief Vérifie si le joueur est proche de la plante de vitesse.
         */
        void joueurProchePlanteVitesse();

        /**
         * @brief Fait apparaître un monstre.
         * 
         * @param dt Le temps écoulé depuis la dernière frame.
         */
        void spawnMonstre(float dt);

        /**
         * @brief Fait apparaître une vague de monstres.
         * 
         * @param dt Le temps écoulé depuis la dernière frame.
         */
        void spawnVagueMonstre(float dt);

        /**
         * @brief Met à jour les actions de la souris.
         * 
         * @param dt Le temps écoulé depuis la dernière frame.
         */
        void updateSouris(float dt);

        /**
         * @brief Met à jour les actions du clavier.
         * 
         * @param dt Le temps écoulé depuis la dernière frame.
         */
        void updateClavier(float dt);

        /**
         * @brief Affiche les éléments du jeu.
         */
        void afficher() const;

        /**
         * @brief Obtient l'état du jeu.
         * 
         * @return L'état du jeu.
         */
        bool getEtatJeu() const;

        /**
         * @brief Obtient le nombre de monstres présents dans le jeu.
         * 
         * @return Le nombre de monstres présents dans le jeu.
         */
        int getNbMonstres() const;

        /**
         * @brief Obtient le temps de recharge de la compétence de tir mystique.
         * 
         * @return Le temps de recharge de la compétence de tir mystique.
         */
        float getTirMystiqueCooldown() const;

        /**
         * @brief Obtient le temps de recharge de la compétence de flash.
         * 
         * @return Le temps de recharge de la compétence de flash.
         */
        float getFlashCooldown() const;
};

/**
 * @brief Obtient la carte du jeu.
 * 
 * @return La carte du jeu.
 */
inline const Map& Jeu::getMap() const { return map; }

/**
 * @brief Obtient le joueur.
 * 
 * @return Le joueur.
 */
inline const Joueur& Jeu::getJoueur() const { return joueur; }

/**
 * @brief Obtient les monstres présents dans le jeu.
 * 
 * @return Les monstres présents dans le jeu.
 */
inline const std::vector<Monstre>& Jeu::getMonstres() const { return monstres; }

/**
 * @brief Obtient le nombre de monstres présents dans le jeu.
 * 
 * @return Le nombre de monstres présents dans le jeu.
 */
inline int Jeu::getNbMonstres() const { return nbMonstres; }

/**
 * @brief Obtient une compétence du joueur.
 * 
 * @param indice L'indice de la compétence.
 * @return La compétence correspondante.
 */
inline const Competence* Jeu::getCompetence(int indice) const 
{ 
    if (indice >= 0 && (size_t)indice < competence.size())
    {
        return competence[indice]; 
    }
    else
    {
        return nullptr;
    }
}

/**
 * @brief Obtient la plante de vie.
 * 
 * @return La plante de vie.
 */
inline const PlantVie& Jeu::getPlantVie() const { return plantVie; }

/**
 * @brief Obtient la plante de vitesse.
 * 
 * @return La plante de vitesse.
 */
inline const PlantVitesse& Jeu::getPlantVitesse() const { return plantVitesse; }

/**
 * @brief Obtient le temps de recharge de la compétence de tir mystique.
 * 
 * @return Le temps de recharge de la compétence de tir mystique.
 */
inline float Jeu::getTirMystiqueCooldown() const { return tirMystiqueCooldown; }

/**
 * @brief Obtient le temps de recharge de la compétence de flash.
 * 
 * @return Le temps de recharge de la compétence de flash.
 */
inline float Jeu::getFlashCooldown() const { return flashCooldown; }

/**
 * @brief Obtient l'état du jeu.
 * 
 * @return L'état du jeu.
 */
inline bool Jeu::getEtatJeu() const { return etatJeu; }

#endif