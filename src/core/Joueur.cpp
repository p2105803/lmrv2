#include "Joueur.h"
#include "../extern/CalculTrajectoire.h"
#include "../extern/Point.h"
#include "Monstre.h"

Joueur::Joueur(float posX, float posY, float vie, float vitesse, float boostVitesse) : j_posX(posX), j_posY(posY), j_vie(vie), vitesseJ(vitesse), boostVitesse(boostVitesse) {}

Joueur::Joueur() : 
    j_posX(0.0f), 
    j_posY(0.0f), 
    j_vie(100), 
    vitesseJ(1.0f), 
    boostVitesse(1.5f), 
    boostEndTime(0)
{
}

void Joueur::deplacerSouris(float posX, float posY, const Map &m, const std::vector<Monstre> &monstres, float dt, const PlantVie &plantVie)
{

    Point positionSouris = {posX, posY};
    Point positionJoueur = {getPosX(), getPosY()};
    CalculTrajectoire trajectoireSouris;
    trajectoireSouris.deplacerVersSouris(positionJoueur, positionSouris, m, monstres, dt, vitesseJ);
    setPosX(positionJoueur.x);
    setPosY(positionJoueur.y);
}

void Joueur::activateSpeedBoost()
{
    boostEndTime = std::time(0) + 5;
    boostVitesse = vitesseJ + 5;
}

void Joueur::deplacer(float posX, float posY, const Map &m)
{
    float newX = j_posX + posX;
    float newY = j_posY + posY;
    if (m.estPositionValideClavier(newX, newY))
    {
        j_posX = newX;
        j_posY = newY;
    }
}

void Joueur::gauche(const Map &m, float dt)
{
    deplacer(-1, 0, m);
}

void Joueur::droite(const Map &m, float dt)
{
    deplacer(1, 0, m);
}

void Joueur::haut(const Map &m, float dt)
{
    deplacer(0, -1, m);
}

void Joueur::bas(const Map &m, float dt)
{
    deplacer(0, 1, m);
}