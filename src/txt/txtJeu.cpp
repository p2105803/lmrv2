#include <iostream>
#include <termios.h>
#include <unistd.h>
#include "txtJeu.h"

void txtJeu::txtAff(Jeu &j)
{
    std::cout << "Voici le mode txt\n";
    struct termios old_tio, new_tio;
    unsigned char c;

    tcgetattr(STDIN_FILENO, &old_tio);
    new_tio = old_tio;
    new_tio.c_lflag &= (~ICANON & ~ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &new_tio);
    //system("clear");
    float dt = 0.1f;
    j.afficher();
    while (true) 
    {
        c = std::tolower(static_cast<unsigned char>(getchar())); 

        if (c == 'a') 
            break;

        switch (c)
        {
        case 'z':
        case 's':
        case 'q':
        case 'd':
            j.deplacementJoueur(c,dt);
            //system("clear");
            j.afficher();
            break;
        case 'm':
        case 'l':
        case 'k':
        case 'o':
        case 'f':
        case 'g':
            j.utiliserCompetenceTir(c,dt); // Affichage inclus dedans avec system("clear") a décommenter
            break;
        }
        j.updateClavier(dt);
    }

    tcsetattr(STDIN_FILENO, TCSANOW, &old_tio); // Reset terminal settings
}