#ifndef _SDLJEU_H
#define _SDLJEU_H

#include <SDL2/SDL.h>
#include <vector>
#include <SDL2/SDL_image.h>
#include "../core/Jeu.h"
#include "SDLSprite.h"
#include <SDL2/SDL_mixer.h>

/**
 * @class SDLJeu
 *
 * @brief Classe représentant le jeu SDL.
 *
 * Cette classe gère l'affichage et l'interaction avec le joueur pour le jeu SDL.
 */
class SDLJeu
{

public:
    /**
     * @brief Constructeur par défaut de la classe SDLJeu.
     */
    SDLJeu();

    /**
     * @brief Destructeur de la classe SDLJeu.
     */
    ~SDLJeu();

    /**
     * @brief Boucle principale pour la gestion de la souris.
     */
    void sdlBoucleSouris();

    /**
     * @brief Boucle principale pour la gestion du clavier.
     */
    void sdlBoucleClavier();

    /**
     * @brief Affiche le jeu à l'écran.
     */
    void sdlAff();

    /**
     * @brief Affiche le menu du jeu.
     */
    void sdlMenu();

    /**
     * @brief Affiche le menu de fin du jeu.
     */
    void sdlMenuFin();

    /**
     * @brief Affiche le menu de pause du jeu.
     */
    void sdlMenuPause();

    /**
     * @brief Affiche le premier menu d'aide du jeu.
     */
    void sdlMenuAide1();

    /**
     * @brief Affiche le deuxième menu d'aide du jeu.
     */
    void sdlMenuAide2();

    /**
     * @brief Obtient le choix du joueur.
     * @return Le choix du joueur.
     */
    int getChoixJoueur() const;

    /**
     * @brief Définit le choix du joueur.
     * @param choix Le choix du joueur.
     */
    void setChoixJoueur(int choix);

    /**
     * @brief Obtient la direction du joueur.
     * @return La direction du joueur.
     */
    char getDirection() const;

    void sdlMusique();

private:
    Jeu* jeu; ///< Instance de la classe Jeu.

    int choixJoueur = 0; ///< Choix du joueur.

    char direction='\0'; ///< Direction du joueur.

    SDLSprite im_monstre; ///< Sprite du monstre.

    SDLSprite im_joueurG; ///< Sprite du joueur vers la gauche.
    SDLSprite im_joueurD; ///< Sprite du joueur vers la droite.
    SDLSprite im_joueurB; ///< Sprite du joueur vers le bas.
    SDLSprite im_joueurH; ///< Sprite du joueur vers le haut.

    SDLSprite im_competence; ///< Sprite de la compétence.

    SDLSprite im_mur; ///< Sprite du mur.
    SDLSprite im_grass; ///< Sprite de l'herbe.
    SDLSprite im_planteVie; ///< Sprite de la plante de vie.
    SDLSprite im_plantVitesse; ///< Sprite de la plante de vitesse.

    SDLSprite im_full_vie; ///< Sprite de la vie complète.
    SDLSprite im_4demi_vie; ///< Sprite de 4 demi-vies.
    SDLSprite im_4vies; ///< Sprite de 4 vies.
    SDLSprite im_3demi_vie; ///< Sprite de 3 demi-vies.
    SDLSprite im_3vies; ///< Sprite de 3 vies.
    SDLSprite im_2demi_vie; ///< Sprite de 2 demi-vies.
    SDLSprite im_2vies; ///< Sprite de 2 vies.
    SDLSprite im_1demi_vie; ///< Sprite de 1 demi-vie.
    SDLSprite im_1vie; ///< Sprite de 1 vie.
    SDLSprite im_demi_vie; ///< Sprite de demi-vie.
    SDLSprite im_mort; ///< Sprite de la mort.

    SDLSprite im_menu; ///< Sprite du menu.
    SDLSprite im_menuFin; ///< Sprite du menu de fin.
    SDLSprite im_menuPause; ///< Sprite du menu de pause.

    SDLSprite im_menuAide1; ///< Sprite du premier menu d'aide.
    SDLSprite im_menuAide2; ///< Sprite du deuxième menu d'aide.

    SDL_Window *window; ///< Fenêtre SDL.
    SDL_Renderer *renderer; ///< Renderer SDL.

    Mix_Music *musique; ///< Musique du jeu.
    Mix_Chunk *son; ///< Effect  du jeu.
};

/**
 * @brief Obtient le choix du joueur.
 * @return Le choix du joueur.
 */
inline int SDLJeu::getChoixJoueur() const
{
    return choixJoueur;
}

/**
 * @brief Obtient la direction du joueur.
 * @return La direction du joueur.
 */
inline char SDLJeu::getDirection() const
{
    return direction;
}

/**
 * @brief Définit le choix du joueur.
 * @param choix Le choix du joueur.
 */
inline void SDLJeu::setChoixJoueur(int choix) {
    choixJoueur  = choix;
}

#endif