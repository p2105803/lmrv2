#ifndef _SDLSPRITE_H
#define _SDLSPRITE_H
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

/**
 * @brief La classe SDLSprite représente un sprite SDL.
 * 
 * Cette classe permet de charger une image à partir d'un fichier ou d'une surface,
 * de dessiner le sprite sur un rendu SDL et d'accéder à la texture associée.
 */
class SDLSprite
{
private:
    SDL_Surface *m_surface; /**< La surface SDL du sprite */
    SDL_Texture *m_texture; /**< La texture SDL du sprite */
    bool m_hasChanged; /**< Indique si le sprite a été modifié */

public:
    /**
     * @brief Constructeur par défaut de SDLSprite.
     */
    SDLSprite();

    /**
     * @brief Destructeur de SDLSprite.
     */
    ~SDLSprite();

    /**
     * @brief Constructeur de copie de SDLSprite.
     * 
     * @param im Le sprite à copier.
     */
    SDLSprite(const SDLSprite &im);

    /**
     * @brief Opérateur d'affectation de SDLSprite.
     * 
     * @param im Le sprite à affecter.
     * @return Une référence vers le sprite affecté.
     */
    SDLSprite &operator=(const SDLSprite &im);

    /**
     * @brief Charge une image à partir d'un fichier.
     * 
     * @param filename Le chemin du fichier image.
     * @param renderer Le rendu SDL sur lequel dessiner le sprite.
     */
    void loadFromFile(const char *filename, SDL_Renderer *renderer);

    /**
     * @brief Charge une image à partir de la surface actuelle.
     * 
     * @param renderer Le rendu SDL sur lequel dessiner le sprite.
     */
    void loadFromCurrentSurface(SDL_Renderer *renderer);

    /**
     * @brief Dessine le sprite sur le rendu SDL.
     * 
     * @param renderer Le rendu SDL sur lequel dessiner le sprite.
     * @param x La position horizontale du sprite.
     * @param y La position verticale du sprite.
     * @param w La largeur du sprite (par défaut -1 pour utiliser la largeur de la texture).
     * @param h La hauteur du sprite (par défaut -1 pour utiliser la hauteur de la texture).
     */
    void draw(SDL_Renderer *renderer, int x, int y, int w = -1, int h = -1);

    /**
     * @brief Récupère la texture associée au sprite.
     * 
     * @return Un pointeur vers la texture SDL du sprite.
     */
    SDL_Texture *getTexture() const;

    /**
     * @brief Définit la surface du sprite.
     * 
     * @param surf La surface SDL à associer au sprite.
     */
    void setSurface(SDL_Surface *surf);
};

#endif