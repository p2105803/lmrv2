#include "SDLJeu.h"

const int TAILLE_SPRITE = 24;
const float TAILLE = 1.5;
int initMusique = Mix_Init(0);

SDLJeu::SDLJeu()
{
    jeu = new Jeu();
    direction = 'q';
    if (SDL_Init(SDL_INIT_VIDEO) != 0)
    {
        std::cerr << "Erreur lors de l'initialisation de la SDL : " << SDL_GetError() << std::endl;
        SDL_Quit();
        exit(1);
    }
    int dimx, dimy;
    dimx = jeu->getMap().getDimX();
    dimy = jeu->getMap().getDimY();

    window = SDL_CreateWindow("LMR", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, dimx, dimy, SDL_WINDOW_SHOWN);
    if (window == nullptr)
    {
        std::cerr << "Erreur lors de la creation de la fenetre : " << SDL_GetError() << std::endl;
        SDL_Quit();
        exit(1);
    }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == nullptr)
    {
        std::cerr << "Erreur lors du renderer : " << SDL_GetError() << std::endl;
        SDL_DestroyWindow(window);
        SDL_Quit();
        exit(1);
    }
    int imgFlags = IMG_INIT_PNG | IMG_INIT_JPG;
    if (!(IMG_Init(imgFlags) & imgFlags))
    {
        std::cerr << "Erreur lors de l'initialisation de SDL_image : " << IMG_GetError() << std::endl;
        SDL_Quit();
        exit(1);
    }
    im_joueurG.loadFromFile("data/joueurG.png", renderer);
    im_joueurD.loadFromFile("data/joueurD.png", renderer);
    im_joueurB.loadFromFile("data/joueurB.png", renderer);
    im_joueurH.loadFromFile("data/joueurH.png", renderer);
    im_monstre.loadFromFile("data/monstre.png", renderer);

    im_mur.loadFromFile("data/map.png", renderer);
    im_grass.loadFromFile("data/buisson.png", renderer);
    im_planteVie.loadFromFile("data/PlanteDeVie.png", renderer);
    im_plantVitesse.loadFromFile("data/plantVitesse.png", renderer);

    im_full_vie.loadFromFile("data/fullvie.png", renderer);
    im_4demi_vie.loadFromFile("data/4demivies.png", renderer);
    im_4vies.loadFromFile("data/4vies.png", renderer);
    im_3demi_vie.loadFromFile("data/3demivies.png", renderer);
    im_3vies.loadFromFile("data/3vies.png", renderer);
    im_2demi_vie.loadFromFile("data/2demivies.png", renderer);
    im_2vies.loadFromFile("data/2vies.png", renderer);
    im_1demi_vie.loadFromFile("data/1demievie.png", renderer);
    im_1vie.loadFromFile("data/1vie.png", renderer);
    im_demi_vie.loadFromFile("data/demivie.png", renderer);
    im_mort.loadFromFile("data/mortvie.png", renderer);
    im_competence.loadFromFile("data/FB001.png", renderer);

    im_menu.loadFromFile("data/menu.png", renderer);
    im_menuFin.loadFromFile("data/gameover.png", renderer);
    im_menuPause.loadFromFile("data/pause.png", renderer);

    im_menuAide1.loadFromFile("data/aide1.png", renderer);
    im_menuAide2.loadFromFile("data/aide2.png", renderer);

    Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048);

    musique = Mix_LoadMUS("data/menu.mp3");
    Mix_PlayMusic(musique, -1);

    if (!musique)
    {
        std::cerr << "Erreur lors du chargement de la musique : " << Mix_GetError() << std::endl;
        SDL_Quit();
        exit(1);
    }
    son = Mix_LoadWAV("data/soundeffect.mp3");
    if (!son)
    {
        std::cerr << "Erreur lors du sound effect : " << Mix_GetError() << std::endl;
        SDL_Quit();
        exit(1);
    }
}

SDLJeu::~SDLJeu()
{
    delete jeu;

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

void SDLJeu::sdlMenu()
{

    SDL_Rect destRect{0, 0, jeu->getMap().getDimX(), jeu->getMap().getDimY()};
    SDL_Rect jouerSourisRect, jouerClavierRect, aideRect, quitterRect;

    jouerSourisRect.x = 209 * TAILLE;
    jouerSourisRect.y = 173 * TAILLE;
    jouerSourisRect.w = 228 * TAILLE;
    jouerSourisRect.h = 63 * TAILLE;

    jouerClavierRect.x = 209 * TAILLE;
    jouerClavierRect.y = 255 * TAILLE;
    jouerClavierRect.w = 228 * TAILLE;
    jouerClavierRect.h = 63 * TAILLE;

    quitterRect.x = 335 * TAILLE;
    quitterRect.y = 339 * TAILLE;
    quitterRect.w = 100 * TAILLE;
    quitterRect.h = 58 * TAILLE;

    aideRect.x = 213 * TAILLE;
    aideRect.y = 339 * TAILLE;
    aideRect.w = 100 * TAILLE;
    aideRect.h = 58 * TAILLE;

    while (true)
    {

        SDL_Event events;

        while (SDL_PollEvent(&events))
        {
            if (events.type == SDL_QUIT)
            {
                exit(0);
            }
            else if (events.type == SDL_MOUSEBUTTONDOWN)
            {

                int mouseX, mouseY;
                SDL_GetMouseState(&mouseX, &mouseY);
                SDL_Point mousePoint = {mouseX, mouseY};
                if (SDL_PointInRect(&mousePoint, &jouerSourisRect))
                {
                    choixJoueur = 1;
                    sdlBoucleSouris();
                    break;
                }
                else if (SDL_PointInRect(&mousePoint, &jouerClavierRect))
                {
                    choixJoueur = 2;
                    sdlBoucleClavier();
                    break;
                }
                else if (SDL_PointInRect(&mousePoint, &aideRect))
                {
                    sdlMenuAide1();
                    break;
                }
                else if (SDL_PointInRect(&mousePoint, &quitterRect))
                {
                    exit(0);
                }
            }
        }
        SDL_RenderPresent(renderer);
        SDL_RenderCopy(renderer, im_menu.getTexture(), NULL, &destRect);
    }
}

void SDLJeu::sdlMenuPause()
{
    SDL_Rect destRect{0, 0, jeu->getMap().getDimX(), jeu->getMap().getDimY()};
    SDL_Rect continuerRect, retourVersMenuRect, quitterRect;

    continuerRect.x = 204 * TAILLE;
    continuerRect.y = 168 * TAILLE;
    continuerRect.w = 231 * TAILLE;
    continuerRect.h = 65 * TAILLE;

    retourVersMenuRect.x = 204 * TAILLE;
    retourVersMenuRect.y = 253 * TAILLE;
    retourVersMenuRect.w = 231 * TAILLE;
    retourVersMenuRect.h = 64 * TAILLE;

    quitterRect.x = 203 * TAILLE;
    quitterRect.y = 337 * TAILLE;
    quitterRect.w = 229 * TAILLE;
    quitterRect.h = 65 * TAILLE;

    SDL_RenderClear(renderer);

    while (true)
    {
        SDL_Event events;

        while (SDL_PollEvent(&events))
        {

            if (events.type == SDL_QUIT)
            {
                exit(0);
            }
            else if (events.type == SDL_MOUSEBUTTONDOWN)
            {
                int mouseX, mouseY;
                SDL_GetMouseState(&mouseX, &mouseY);
                SDL_Point mousePoint = {mouseX, mouseY};
                if (SDL_PointInRect(&mousePoint, &continuerRect))
                {
                    if (choixJoueur == 2)
                    {
                        sdlBoucleClavier();
                        break;
                    }
                    else
                    {
                        sdlBoucleSouris();
                        break;
                    }
                }
                else if (SDL_PointInRect(&mousePoint, &retourVersMenuRect))
                {
                    delete jeu;
                    jeu = new Jeu();
                    choixJoueur = 0;
                    sdlMenu();
                    break;
                }
                else if (SDL_PointInRect(&mousePoint, &quitterRect))
                {
                    exit(0);
                }
            }
        }
        SDL_RenderPresent(renderer);
        SDL_RenderCopy(renderer, im_menuPause.getTexture(), NULL, &destRect);
    }
}

void SDLJeu::sdlMenuFin()
{
    SDL_Rect destRect{0, 0, jeu->getMap().getDimX(), jeu->getMap().getDimY()};
    SDL_Rect reJouerRect, retourVersMenuRect, quitterRect;

    reJouerRect.x = 206 * TAILLE;
    reJouerRect.y = 168 * TAILLE;
    reJouerRect.w = 231 * TAILLE;
    reJouerRect.h = 65 * TAILLE;

    retourVersMenuRect.x = 204 * TAILLE;
    retourVersMenuRect.y = 253 * TAILLE;
    retourVersMenuRect.w = 231 * TAILLE;
    retourVersMenuRect.h = 65 * TAILLE;

    quitterRect.x = 202 * TAILLE;
    quitterRect.y = 337 * TAILLE;
    quitterRect.w = 230 * TAILLE;
    quitterRect.h = 66 * TAILLE;

    SDL_RenderClear(renderer);

    while (true)
    {
        SDL_Event events;

        while (SDL_PollEvent(&events))
        {
            if (events.type == SDL_QUIT)
            {
                exit(0);
            }
            else if (events.type == SDL_MOUSEBUTTONDOWN)
            {
                int mouseX, mouseY;
                SDL_GetMouseState(&mouseX, &mouseY);
                SDL_Point mousePoint = {mouseX, mouseY};
                if (SDL_PointInRect(&mousePoint, &reJouerRect))
                {
                    if (choixJoueur == 2)
                    {
                        delete jeu;
                        jeu = new Jeu();
                        sdlBoucleClavier();
                        break;
                    }
                    else
                    {
                        delete jeu;
                        jeu = new Jeu();
                        sdlBoucleSouris();
                        break;
                    }
                }
                else if (SDL_PointInRect(&mousePoint, &retourVersMenuRect))
                {
                    delete jeu;
                    jeu = new Jeu();
                    choixJoueur = 0;
                    sdlMenu();
                    break;
                }
                else if (SDL_PointInRect(&mousePoint, &quitterRect))
                {
                    delete jeu;
                    jeu = nullptr;
                    exit(0);
                }
            }
        }
        SDL_RenderPresent(renderer);
        SDL_RenderCopy(renderer, im_menuFin.getTexture(), NULL, &destRect);
    }
}

void SDLJeu::sdlMenuAide1()
{
    SDL_Rect destRect{0, 0, jeu->getMap().getDimX(), jeu->getMap().getDimY()};
    SDL_Rect continuerRect;

    continuerRect.x = 437 * TAILLE;
    continuerRect.y = 395 * TAILLE;
    continuerRect.w = 126 * TAILLE;
    continuerRect.h = 50 * TAILLE;

    while (true)
    {
        SDL_Event events;

        while (SDL_PollEvent(&events))
        {
            if (events.type == SDL_QUIT)
            {
                exit(0);
            }
            else if (events.type == SDL_MOUSEBUTTONDOWN)
            {
                int mouseX, mouseY;
                SDL_GetMouseState(&mouseX, &mouseY);
                SDL_Point mousePoint = {mouseX, mouseY};
                if (SDL_PointInRect(&mousePoint, &continuerRect))
                {
                    sdlMenuAide2();
                    break;
                }
            }
        }
        SDL_RenderPresent(renderer);
        SDL_RenderCopy(renderer, im_menuAide1.getTexture(), NULL, &destRect);
    }
}

void SDLJeu::sdlMenuAide2()
{
    SDL_Rect destRect{0, 0, jeu->getMap().getDimX(), jeu->getMap().getDimY()};
    SDL_Rect retourRect, comprisRect;

    retourRect.x = 73 * TAILLE;
    retourRect.y = 394 * TAILLE;
    retourRect.w = 126 * TAILLE;
    retourRect.h = 49 * TAILLE;

    comprisRect.x = 435 * TAILLE;
    comprisRect.y = 394 * TAILLE;
    comprisRect.w = 126 * TAILLE;
    comprisRect.h = 49 * TAILLE;

    while (true)
    {
        SDL_Event events;

        while (SDL_PollEvent(&events))
        {
            if (events.type == SDL_QUIT)
            {
                exit(0);
            }
            else if (events.type == SDL_MOUSEBUTTONDOWN)
            {
                int mouseX, mouseY;
                SDL_GetMouseState(&mouseX, &mouseY);
                SDL_Point mousePoint = {mouseX, mouseY};
                if (SDL_PointInRect(&mousePoint, &retourRect))
                {
                    sdlMenuAide1();
                    break;
                }
                else if (SDL_PointInRect(&mousePoint, &comprisRect))
                {
                    sdlMenu();
                    break;
                }
            }
        }
        SDL_RenderPresent(renderer);
        SDL_RenderCopy(renderer, im_menuAide2.getTexture(), NULL, &destRect);
    }
}

void SDLJeu::sdlAff()
{
    SDL_SetRenderDrawColor(renderer, 230, 240, 255, 255);
    SDL_RenderClear(renderer);
    const Joueur &joueur = jeu->getJoueur();
    const std::vector<Monstre> &monstres = jeu->getMonstres();
    PlantVie plantVie = jeu->getPlantVie();
    PlantVitesse plantVitesse = jeu->getPlantVitesse();

    SDL_RenderCopy(renderer, im_mur.getTexture(), NULL, NULL);

    if (jeu->getCompetence(1) != nullptr)
    {
        int roundedXCompetence = static_cast<int>(std::round(jeu->getCompetence(1)->getPosX() * TAILLE_SPRITE));
        int roundedYCompetence = static_cast<int>(std::round(jeu->getCompetence(1)->getPosY() * TAILLE_SPRITE));
        im_competence.draw(renderer, roundedXCompetence, roundedYCompetence, TAILLE_SPRITE, TAILLE_SPRITE);
    }
    int roundedX = static_cast<int>(std::round(jeu->getJoueur().getPosX() * TAILLE_SPRITE));
    int roundedY = static_cast<int>(std::round(jeu->getJoueur().getPosY() * TAILLE_SPRITE));
    switch (direction)
    {
    case 'q':
        im_joueurG.draw(renderer, roundedX, roundedY, TAILLE_SPRITE, TAILLE_SPRITE);
        break;
    case 'd':
        im_joueurD.draw(renderer, roundedX, roundedY, TAILLE_SPRITE, TAILLE_SPRITE);
        break;
    case 'z':
        im_joueurH.draw(renderer, roundedX, roundedY, TAILLE_SPRITE, TAILLE_SPRITE);
        break;
    case 's':
        im_joueurB.draw(renderer, roundedX, roundedY, TAILLE_SPRITE, TAILLE_SPRITE);
        break;
    default:
        im_joueurG.draw(renderer, roundedX, roundedY, TAILLE_SPRITE, TAILLE_SPRITE);
        break;
    }

    int roundedXPlanteVie = static_cast<int>(std::round(plantVie.getPosX() * TAILLE_SPRITE));
    int roundedYplantVie = static_cast<int>(std::round(plantVie.getPosY() * TAILLE_SPRITE));

    if (std::time(0) - plantVie.getUsedTime() >= 30)
    {
        plantVie.setActive(true);
    }
    if (plantVie.estActif())
        im_planteVie.draw(renderer, roundedXPlanteVie, roundedYplantVie, TAILLE_SPRITE, TAILLE_SPRITE);

    int roundedXPlanteVitesse = static_cast<int>(std::round(plantVitesse.getPosX() * TAILLE_SPRITE));
    int roundedYplantVitesse = static_cast<int>(std::round(plantVitesse.getPosY() * TAILLE_SPRITE));

    if (std::time(0) - plantVitesse.getUsedTime() >= 30)
    {
        plantVitesse.setActive(true);
    }
    if (plantVitesse.estActif())
        im_plantVitesse.draw(renderer, roundedXPlanteVitesse, roundedYplantVitesse, TAILLE_SPRITE, TAILLE_SPRITE);

    for (size_t i = 0; i < monstres.size(); i++)
    {
        int roundedXMonstre = static_cast<int>(std::round(monstres[i].getPosX() * TAILLE_SPRITE));
        int roundedYMonstre = static_cast<int>(std::round(monstres[i].getPosY() * TAILLE_SPRITE));
        im_monstre.draw(renderer, roundedXMonstre, roundedYMonstre, TAILLE_SPRITE, TAILLE_SPRITE);
    }
    if (joueur.getVie() > 45)
        im_full_vie.draw(renderer, TAILLE_SPRITE, 2, 71 * TAILLE, TAILLE_SPRITE);
    else if (joueur.getVie() > 40)
        im_4demi_vie.draw(renderer, TAILLE_SPRITE, 2, 71 * TAILLE, TAILLE_SPRITE);
    else if (joueur.getVie() > 35)
        im_4vies.draw(renderer, TAILLE_SPRITE, 2, 71 * TAILLE, TAILLE_SPRITE);
    else if (joueur.getVie() > 30)
        im_3demi_vie.draw(renderer, TAILLE_SPRITE, 2, 71 * TAILLE, TAILLE_SPRITE);
    else if (joueur.getVie() > 25)
        im_3vies.draw(renderer, TAILLE_SPRITE, 2, 71 * TAILLE, TAILLE_SPRITE);
    else if (joueur.getVie() > 20)
        im_2demi_vie.draw(renderer, TAILLE_SPRITE, 2, 71 * TAILLE, TAILLE_SPRITE);
    else if (joueur.getVie() > 15)
        im_2vies.draw(renderer, TAILLE_SPRITE, 2, 71 * TAILLE, TAILLE_SPRITE);
    else if (joueur.getVie() > 10)
        im_1demi_vie.draw(renderer, TAILLE_SPRITE, 2, 71 * TAILLE, TAILLE_SPRITE);
    else if (joueur.getVie() > 5)
        im_1vie.draw(renderer, TAILLE_SPRITE, 2, 71 * TAILLE, TAILLE_SPRITE);
    else if (joueur.getVie() > 0)
        im_demi_vie.draw(renderer, TAILLE_SPRITE, 2, 71 * TAILLE, TAILLE_SPRITE);
    else
        im_mort.draw(renderer, TAILLE_SPRITE, 2, 71 * TAILLE, TAILLE_SPRITE);
}

void SDLJeu::sdlBoucleSouris()
{

    SDL_Event events;
    bool quit = false;
    float cibleX = -1, cibleY = -1;
    int cibleTirX = -1, cibleTirY = -1;
    bool flashActive = false;
    bool tirActive = false;

    Uint64 currentTick = SDL_GetTicks();
    double dt = 0.0;
    Uint64 lastTick = currentTick;
    while (!quit)
    {
        lastTick = currentTick;
        currentTick = SDL_GetTicks();
        dt = (double)(currentTick - lastTick) / 1000.0;
        if (dt > 0.01)
        {
            jeu->updateSouris(dt);
            jeu->spawnMonstre(dt);
            jeu->spawnVagueMonstre(dt);
            lastTick = currentTick;
        }

        while (SDL_PollEvent(&events))
        {
            if (events.type == SDL_QUIT)
            {
                quit = true;
                exit(0);
            }
            else if (events.type == SDL_MOUSEBUTTONDOWN)
            {
                if (events.button.button == SDL_BUTTON_RIGHT)
                {
                    cibleX = events.button.x / TAILLE_SPRITE;
                    cibleY = events.button.y / TAILLE_SPRITE;
                    jeu->clickJoueurSurPlanteVie(cibleX, cibleY);
                    jeu->clickJoueurSurPlanteVitesse(cibleX, cibleY);
                }
                else if (events.button.button == SDL_BUTTON_LEFT)
                {
                    if (jeu->getCompetence(1) == nullptr || jeu->getCompetence(1)->getActive() == false)
                    {
                        SDL_GetMouseState(&cibleTirX, &cibleTirY);
                        cibleTirX /= TAILLE_SPRITE;
                        cibleTirY /= TAILLE_SPRITE;
                        tirActive = true;
                        Mix_PlayChannel(-1, son, 0);
                    }
                }
            }
            else if (events.type == SDL_KEYDOWN)
            {
                switch (events.key.keysym.scancode)
                {
                case SDL_SCANCODE_F:
                    if (jeu->getCompetence(0) == nullptr || jeu->getCompetence(0)->getActive() == false)
                    {
                        SDL_GetMouseState(&cibleTirX, &cibleTirY);
                        cibleTirX /= TAILLE_SPRITE;
                        cibleTirY /= TAILLE_SPRITE;
                        flashActive = true;
                    }
                    break;
                case SDL_SCANCODE_P:
                    sdlMenuPause();
                    break;

                default:
                    break;
                }
            }
        }

        if (cibleTirX != -1 && tirActive)
        {
            jeu->utiliserCompetenceSouris('g', cibleTirX, cibleTirY, dt);
            if (!jeu->getCompetence(1)->getActive())
            {
                cibleTirX = -1;
                cibleTirY = -1;
                tirActive = false;
            }
        }

        if (cibleTirX != -1 && flashActive)
        {
            jeu->utiliserCompetenceSouris('f', cibleTirX, cibleTirY, dt);
            if (!jeu->getCompetence(0)->getActive())
            {
                cibleX = -1;
                cibleY = -1;
                flashActive = false;
            }
        }

        if (cibleX != -1 && !flashActive)
        {
            jeu->deplacementJoueurSouris(cibleX, cibleY, dt);
            if (jeu->getJoueur().getPosX() == cibleX && jeu->getJoueur().getPosY() == cibleY)
            {
                cibleX = -1;
                cibleY = -1;
            }
        }
        else
        {
            cibleX = -1;
            cibleY = -1;
        }

        flashActive = false;

        sdlAff();
        SDL_RenderPresent(renderer);

        if (jeu->getEtatJeu() == false)
        {
            sdlMenuFin();
            break;
        }
    }
}

void SDLJeu::sdlBoucleClavier()
{
    SDL_Event events;
    bool quit = false;
    bool flashActive = false;
    bool tirActive = false;

    char dir = '\0';

    Uint64 currentTick = SDL_GetTicks();
    double dt = 0.0;
    Uint64 lastTick = currentTick;
    Uint64 lastMonsterMovementTick = currentTick;
    const double tickRate = 0.25;

    while (!quit)
    {
        lastTick = currentTick;
        currentTick = SDL_GetTicks();
        dt = (double)(currentTick - lastTick) / 1000.0;

        jeu->spawnMonstre(dt);
        jeu->spawnVagueMonstre(dt);

        while (SDL_PollEvent(&events))
        {

            if (events.type == SDL_QUIT)
            {
                quit = true;
                exit(0);
            }
            else if (events.type == SDL_KEYDOWN)
            {
                jeu->joueurProchePlanteVie();
                jeu->joueurProchePlanteVitesse();
                switch (events.key.keysym.scancode)
                {
                case SDL_SCANCODE_UP:
                    jeu->deplacementJoueur('z', dt);
                    direction = 'z';
                    break;
                case SDL_SCANCODE_DOWN:
                    jeu->deplacementJoueur('s', dt);
                    direction = 's';
                    break;
                case SDL_SCANCODE_LEFT:
                    jeu->deplacementJoueur('q', dt);
                    direction = 'q';
                    break;
                case SDL_SCANCODE_RIGHT:
                    jeu->deplacementJoueur('d', dt);
                    direction = 'd';
                    break;
                case SDL_SCANCODE_F:
                    dir = getDirection();
                    flashActive = true;
                    break;
                case SDL_SCANCODE_G:
                    if (jeu->getCompetence(1) == nullptr || jeu->getCompetence(1)->getActive() == false)
                    {
                        tirActive = true;
                        dir = getDirection();
                        Mix_PlayChannel(-1, son, 0);
                    }
                    break;
                case SDL_SCANCODE_P:
                    sdlMenuPause();
                    break;
                default:
                    break;
                }
            }
        }

        if (tirActive)
        {
            jeu->utiliserCompetenceTir(dir, dt);
            if (!jeu->getCompetence(1)->getActive())
            {
                tirActive = false;
            }
        }

        if (flashActive)
        {
            jeu->utiliserCompetenceFlash(dir, dt);
            flashActive = false;
        }

        if (currentTick - lastMonsterMovementTick >= tickRate * 1000)
        {
            jeu->updateClavier(dt);
            lastMonsterMovementTick = currentTick;
        }

        sdlAff();
        SDL_RenderPresent(renderer);

        if (jeu->getEtatJeu() == false)
        {
            sdlMenuFin();
            break;
        }
    }
}
