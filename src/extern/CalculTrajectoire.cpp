#include "CalculTrajectoire.h"
#include <cstdlib>
#include <cmath>
#include <queue>
#include <vector>
#include <algorithm>
#include <unistd.h>

using namespace std;

std::vector<std::pair<int, int>> CalculTrajectoire::trouverChemin(Point &a, const Point &b, const Map &map, std::vector<Monstre> monstres, int indiceMonstre)
{
    int iterations = 0;

    int departX = a.x;
    int departY = a.y;

    int arriveeX = b.x;
    int arriveeY = b.y;

    std::priority_queue<Noeud> frontier;

    std::vector<std::vector<bool>> explored(map.getDimY(), std::vector<bool>(map.getDimX(), false));

    frontier.push(Noeud(departX, departY, 0, std::sqrt(std::pow(arriveeX - departX, 2) + std::pow(arriveeY - departY, 2))));

    std::vector<std::vector<std::pair<int, int>>> cameFrom(map.getDimY(), std::vector<std::pair<int, int>>(map.getDimX(), {-1, -1}));

    while (!frontier.empty())
    {
        if (iterations++ > Noeud::MAX_ITERATIONS)
        {
            return {};
        }
        Noeud current = frontier.top();
        frontier.pop();

        if (explored[current.y][current.x])
            continue;

        if (current.x == arriveeX && current.y == arriveeY)
        {
            std::vector<std::pair<int, int>> chemin;
            while (current.x != departX || current.y != departY)
            {
                chemin.push_back(std::make_pair(current.x, current.y));
                int nextX = cameFrom[current.y][current.x].first;
                int nextY = cameFrom[current.y][current.x].second;
                current.x = nextX;
                current.y = nextY;
            }
            return chemin;
        }

        explored[current.y][current.x] = true;

        for (int dx = -1; dx <= 1; ++dx)
        {
            for (int dy = -1; dy <= 1; ++dy)
            {
                if (dx != 0 && dy != 0)
                    continue;

                int nextX = current.x + dx;
                int nextY = current.y + dy;

                if (nextX >= 0 && nextX < map.getDimX() && nextY >= 0 && nextY < map.getDimY())
                {
                    bool monstrePresent = false;
                    for (size_t i = 0; i < monstres.size(); i++)
                    {
                        if ((int)i != indiceMonstre)
                        {
                            if (std::round(monstres[i].getPosX() - current.x) > 0.5 && std::round(monstres[i].getPosY() - current.y) > 0.5)
                            {
                                float dx = monstres[i].getPosX() - current.x;
                                float dy = monstres[i].getPosY() - current.y;
                                float distance = sqrt(dx * dx + dy * dy);
                                if (distance < 1.f)
                                {
                                    continue;
                                }
                            }
                        }
                    }

                    if (monstrePresent)
                        continue;

                    if (map.getTerrainAt(nextX, nextY) != '#' && map.getTerrainAt(nextX, nextY) != '2' && !explored[nextY][nextX])
                    {
                        double newCost = current.coutChemin + (dx != 0 && dy != 0 ? sqrt(2) : 1);

                        if (newCost < current.coutChemin || !explored[nextY][nextX])
                        {
                            cameFrom[nextY][nextX] = std::make_pair(current.x, current.y);
                            frontier.push(Noeud(nextX, nextY, newCost, std::sqrt(std::pow(arriveeX - nextX, 2) + std::pow(arriveeY - nextY, 2))));
                        }
                    }
                }
            }
        }
    }

    return {};
}

void CalculTrajectoire::deplacerVersModeSouris(Point &a, const Point &b, const Map &map, std::vector<Monstre> monstres, float dt, float vitesseM, int indiceMonstre)
{
    std::vector<std::pair<int, int>> chemin = trouverChemin(a, b, map, monstres, indiceMonstre);

    if (chemin.empty())
    {
        return;
    }

    size_t currentIndex = 0;

    while (currentIndex <= chemin.size())
    {
        std::pair<int, int> currentPoint = chemin[currentIndex];

        float dx = currentPoint.first - a.x;
        float dy = currentPoint.second - a.y;

        float length = std::sqrt(dx * dx + dy * dy);
        dx /= length;
        dy /= length;

        float newX = a.x + dx * dt * vitesseM;
        float newY = a.y + dy * dt * vitesseM;

        if (map.estPositionValide((int)newX, (int)newY))
        {
            a.x = newX;
            a.y = newY;

            if (std::abs(a.x - currentPoint.first) < 0.2f && std::abs(a.y - currentPoint.second) < 0.2f)
            {
                a.x = currentPoint.first;
                a.y = currentPoint.second;
                currentIndex++;
            }
            break;
        }
        else
        {
            float adjustment = 0.1f;
            bool validPositionFound = false;

            for (int i = 0; i < 4; i++)
            {
                float adjustedDx = dx;
                float adjustedDy = dy;

                switch (i)
                {
                case 0:
                    adjustedDy -= adjustment;
                    break;
                case 1:
                    adjustedDy += adjustment;
                    break;
                case 2:
                    adjustedDx -= adjustment;
                    break;
                case 3:
                    adjustedDx += adjustment;
                    break;
                }

                float adjustedNewX = a.x + adjustedDx * dt * vitesseM;
                float adjustedNewY = a.y + adjustedDy * dt * vitesseM;

                if (map.estPositionValide((int)adjustedNewX, (int)adjustedNewY))
                {
                    a.x = adjustedNewX;
                    a.y = adjustedNewY;
                    validPositionFound = true;
                    break;
                }
            }

            if (!validPositionFound)
            {
                currentIndex++;
            }
        }
    }
    chemin.clear();
}

void CalculTrajectoire::deplacerVersModeClavier(Point &a, const Point &b, const Map &map, std::vector<Monstre> monstres, float dt, float vitesseM, int indiceMonstre)
{
    std::vector<std::pair<int, int>> chemin = trouverChemin(a, b, map, monstres, indiceMonstre);

    if (chemin.empty())
    {
        return;
    }

    for (size_t i = 0; i < chemin.size() - 1; ++i)
    {
        auto &depart = chemin[i];
        auto &arrivee = chemin[i + 1];

        int steps = std::max(std::abs(arrivee.first - depart.first), std::abs(arrivee.second - depart.second));

        for (int step = 0; step < steps; ++step)
        {
            float t = static_cast<float>(step) / steps;
            a.x = depart.first + t * (arrivee.first - depart.first);
            a.y = depart.second + t * (arrivee.second - depart.second);
        }
    }

    a.x = chemin.back().first;
    a.y = chemin.back().second;
}

std::vector<std::pair<int, int>> CalculTrajectoire::trouverCheminSouris(Point &a, const Point &b, const Map &map, std::vector<Monstre> monstres)
{
    int iterations = 0;

    int departX = a.x;
    int departY = a.y;

    int arriveeX = b.x;
    int arriveeY = b.y;

    std::priority_queue<Noeud> frontier;

    std::vector<std::vector<bool>> explored(map.getDimY(), std::vector<bool>(map.getDimX(), false));

    frontier.push(Noeud(departX, departY, 0, std::sqrt(std::pow(arriveeX - departX, 2) + std::pow(arriveeY - departY, 2))));

    std::vector<std::vector<std::pair<int, int>>> cameFrom(map.getDimY(), std::vector<std::pair<int, int>>(map.getDimX(), {-1, -1}));

    while (!frontier.empty())
    {
        if (iterations++ > Noeud::MAX_ITERATIONS)
        {
            return {};
        }
        Noeud current = frontier.top();
        frontier.pop();

        if (explored[current.y][current.x])
            continue;

        if (current.x == arriveeX && current.y == arriveeY)
        {
            std::vector<std::pair<int, int>> chemin;
            while (current.x != departX || current.y != departY)
            {
                chemin.push_back(std::make_pair(current.x, current.y));
                int nextX = cameFrom[current.y][current.x].first;
                int nextY = cameFrom[current.y][current.x].second;
                current.x = nextX;
                current.y = nextY;
            }
            return chemin;
        }

        explored[current.y][current.x] = true;

        for (int dx = -1; dx <= 1; ++dx)
        {
            for (int dy = -1; dy <= 1; ++dy)
            {
                if (dx != 0 && dy != 0)
                    continue;

                int nextX = current.x + dx;
                int nextY = current.y + dy;

                if (nextX >= 0 && nextX < map.getDimX() && nextY >= 0 && nextY < map.getDimY())
                {
                    bool monstrePresent = false;
                    for (size_t i = 0; i < monstres.size(); i++)
                    {
                        if (monstres[i].getPosX() == nextX && monstres[i].getPosY() == nextY)
                        {
                            monstrePresent = true;
                            break;
                        }
                    }

                    if (monstrePresent)
                        continue;

                    if (map.getTerrainAt(nextX, nextY) != '#' && map.getTerrainAt(current.x, nextY) != '#' && map.getTerrainAt(nextX, current.y) != '#' && !explored[nextY][nextX])
                    {
                        double newCost = current.coutChemin + (dx != 0 && dy != 0 ? sqrt(2) : 1);

                        if (newCost < current.coutChemin || !explored[nextY][nextX])
                        {
                            cameFrom[nextY][nextX] = std::make_pair(current.x, current.y);
                            frontier.push(Noeud(nextX, nextY, newCost, std::sqrt(std::pow(arriveeX - nextX, 2) + std::pow(arriveeY - nextY, 2))));
                        }
                    }
                }
            }
        }
    }
    return {};
}

void CalculTrajectoire::deplacerVersSouris(Point &a, const Point &b, const Map &map, std::vector<Monstre> monstres, float dt, float vitesseJ)
{
    std::vector<std::pair<int, int>> chemin = trouverCheminSouris(a, b, map, monstres);

    if (chemin.empty())
    {
        return;
    }

    size_t currentIndex = 0;

    while (currentIndex <= chemin.size())
    {
        std::pair<int, int> currentPoint = chemin[currentIndex];

        float dx = currentPoint.first - a.x;
        float dy = currentPoint.second - a.y;

        float length = std::sqrt(dx * dx + dy * dy);
        dx /= length;
        dy /= length;

        float newX = a.x + dx * dt * vitesseJ;
        float newY = a.y + dy * dt * vitesseJ;

        if (map.estPositionValide((int)newX, (int)newY))
        {
            a.x = newX;
            a.y = newY;

            if (std::abs(a.x - currentPoint.first) < 0.2f && std::abs(a.y - currentPoint.second) < 0.2f)
            {
                a.x = currentPoint.first;
                a.y = currentPoint.second;
                currentIndex++;
            }
            break;
        }
        else
        {
            float adjustment = 0.1f;
            bool validPositionFound = false;

            for (int i = 0; i < 4; i++)
            {
                float adjustedDx = dx;
                float adjustedDy = dy;

                switch (i)
                {
                case 0:
                    adjustedDy -= adjustment;
                    break;
                case 1:
                    adjustedDy += adjustment;
                    break;
                case 2:
                    adjustedDx -= adjustment;
                    break;
                case 3:
                    adjustedDx += adjustment;
                    break;
                }

                float adjustedNewX = a.x + adjustedDx * dt * vitesseJ;
                float adjustedNewY = a.y + adjustedDy * dt * vitesseJ;

                if (map.estPositionValide((int)adjustedNewX, (int)adjustedNewY))
                {
                    a.x = adjustedNewX;
                    a.y = adjustedNewY;
                    validPositionFound = true;
                    break;
                }
            }

            if (!validPositionFound)
            {
                currentIndex++;
            }
        }
    }
    chemin.clear();
}
