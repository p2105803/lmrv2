#ifndef _CALCULTRAJECTOIRE_H
#define _CALCULTRAJECTOIRE_H

#include <iostream>
#include <vector>
#include "Point.h"
#include "../core/Map.h"
#include "../core/Monstre.h"

using namespace std;

/**
 * @class Noeud
 *
 * @brief Représente un noeud dans le calcul de trajectoire.
 *
 * Cette classe représente un noeud utilisé dans le calcul de trajectoire.
 * Chaque noeud a une position (x, y), un coût de chemin et un coût estimé.
 */
class Noeud
{
public:
    float x; ///< La coordonnée x du noeud.
    float y; ///< La coordonnée y du noeud.
    double coutChemin; ///< Le coût du chemin pour atteindre ce noeud.
    double coutEstime; ///< Le coût estimé pour atteindre la destination depuis ce noeud.

    static const int MAX_ITERATIONS = 500; ///< Le nombre maximum d'itérations.

    Noeud() = default;
    Noeud(float _x, float _y, double _coutChemin, double _coutEstime)
        : x(_x), y(_y), coutChemin(_coutChemin), coutEstime(_coutEstime) {}

    /**
     * @brief Opérateur de comparaison pour la file de priorité.
     *
     * Cet opérateur compare deux noeuds en fonction de leur coût total (chemin + estimation).
     * Il est utilisé pour déterminer la priorité dans la file de priorité utilisée dans l'algorithme de recherche de chemin.
     *
     * @param other Le noeud à comparer.
     * @return true si le noeud courant a un coût total plus grand que le noeud other, false sinon.
     */
    bool operator<(const Noeud &other) const
    {
        return (coutChemin + coutEstime) > (other.coutChemin + other.coutEstime);
    }
};

/**
 * @struct CalculTrajectoire
 *
 * @brief Représente les fonctions de calcul de trajectoire.
 *
 * Cette structure contient les fonctions nécessaires pour calculer une trajectoire.
 */
struct CalculTrajectoire
{
    /**
     * @brief Trouve le chemin entre deux points.
     *
     * Cette fonction trouve le chemin entre le point a et le point b sur la carte map.
     * Elle utilise l'algorithme de recherche de chemin A*.
     *
     * @param a Le point de départ.
     * @param b Le point de destination.
     * @param map La carte sur laquelle se déroule le calcul de trajectoire.
     * @param monstres La liste des monstres présents sur la carte.
     * @param indiceMonstre L'indice du monstre pour lequel calculer la trajectoire.
     * @return Le chemin trouvé sous la forme d'une liste de coordonnées (x, y).
     */
    std::vector<std::pair<int, int>> trouverChemin(Point &a, const Point &b, const Map &map, std::vector<Monstre> monstres, int indiceMonstre);

    /**
     * @brief Trouve le chemin entre deux points en mode souris.
     *
     * Cette fonction trouve le chemin entre le point a et le point b sur la carte map en mode souris.
     * Elle utilise l'algorithme de recherche de chemin A*.
     *
     * @param a Le point de départ.
     * @param b Le point de destination.
     * @param map La carte sur laquelle se déroule le calcul de trajectoire.
     * @param monstres La liste des monstres présents sur la carte.
     * @return Le chemin trouvé sous la forme d'une liste de coordonnées (x, y).
     */
    std::vector<std::pair<int, int>> trouverCheminSouris(Point &a, const Point &b, const Map &map, std::vector<Monstre> monstres);

    /**
     * @brief Déplace l'entité vers le point de destination en mode souris.
     *
     * Cette fonction déplace l'entité représentée par le point a vers le point de destination b sur la carte map en mode souris.
     * Elle utilise l'algorithme de recherche de chemin A* pour trouver la trajectoire.
     *
     * @param a Le point de départ.
     * @param b Le point de destination.
     * @param map La carte sur laquelle se déroule le déplacement.
     * @param monstres La liste des monstres présents sur la carte.
     * @param dt Le temps écoulé depuis la dernière mise à jour.
     * @param vitesseM La vitesse de déplacement du monstre.
     * @param indiceMonstre L'indice du monstre pour lequel effectuer le déplacement.
     */
    void deplacerVersModeSouris(Point &a, const Point &b, const Map &map, std::vector<Monstre> monstres, float dt, float vitesseM, int indiceMonstre);

    /**
     * @brief Déplace l'entité vers le point de destination en mode clavier.
     *
     * Cette fonction déplace l'entité représentée par le point a vers le point de destination b sur la carte map en mode clavier.
     * Elle utilise l'algorithme de recherche de chemin A* pour trouver la trajectoire.
     *
     * @param a Le point de départ.
     * @param b Le point de destination.
     * @param map La carte sur laquelle se déroule le déplacement.
     * @param monstres La liste des monstres présents sur la carte.
     * @param dt Le temps écoulé depuis la dernière mise à jour.
     * @param vitesseM La vitesse de déplacement du monstre.
     * @param indiceMonstre L'indice du monstre pour lequel effectuer le déplacement.
     */
    void deplacerVersModeClavier(Point &a, const Point &b, const Map &map, std::vector<Monstre> monstres, float dt, float vitesseM, int indiceMonstre);

    /**
     * @brief Déplace l'entité vers le point de destination en suivant la souris.
     *
     * Cette fonction déplace l'entité représentée par le point a vers le point de destination b sur la carte map en suivant la souris.
     *
     * @param a Le point de départ.
     * @param b Le point de destination.
     * @param map La carte sur laquelle se déroule le déplacement.
     * @param monstres La liste des monstres présents sur la carte.
     * @param dt Le temps écoulé depuis la dernière mise à jour.
     * @param vitesseJ La vitesse de déplacement du joueur.
     */
    void deplacerVersSouris(Point &a, const Point &b, const Map &map, std::vector<Monstre> monstres, float dt, float vitesseJ);
};

#endif