#include <iostream>
#include <iostream>
#include <termios.h>
#include <unistd.h>
#include "core/Jeu.h"
#include "txt/txtJeu.h"
#include "sdl2/SDLJeu.h"

int main(int argc, char *argv[])
{
    srand(time(NULL));
    int option;
    Jeu jeu;
    // system("clear");
    std::cout << "Menu:\n";
    std::cout << "1. Mode Txt \n";
    std::cout << "2. Mode SDL2 \n";
    std::cout << "3. Quitter\n";
    std::cout << "Enter votre choix: ";
    std::cin >> option;
    txtJeu txt;
    SDLJeu jeuSdl;

    switch (option)
    {
    case 1:
        txt.txtAff(jeu);
        break;
    case 2:
        jeuSdl.sdlMenu();
        break;
    default:
        std::cout << "Choix invalide\n";
        break;
    }
    return 0;
}