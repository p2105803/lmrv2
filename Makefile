all: bin/main
	./bin/main

bin/main: obj/main.o obj/txtJeu.o obj/SDLJeu.o obj/SDLSprite.o obj/Jeu.o obj/Map.o obj/Joueur.o obj/Competence.o obj/Flash.o obj/TirMystique.o obj/Monstre.o obj/CalculTrajectoire.o 
	g++ -Wall -g obj/main.o obj/txtJeu.o obj/SDLJeu.o obj/SDLSprite.o obj/Jeu.o obj/Map.o obj/Joueur.o obj/Monstre.o obj/Competence.o obj/Flash.o obj/TirMystique.o obj/CalculTrajectoire.o -o bin/main -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_mixer -I/usr/local/include/SDL2

obj/main.o: src/main.cpp src/core/Jeu.h
	g++ -Wall -g -c src/main.cpp -o obj/main.o 

obj/txtJeu.o : src/txt/txtJeu.cpp src/txt/txtJeu.h src/core/Jeu.h
	g++ -Wall -g -c src/txt/txtJeu.cpp -o obj/txtJeu.o

obj/SDLJeu.o : src/sdl2/SDLJeu.cpp src/sdl2/SDLJeu.h src/core/Jeu.h src/sdl2/SDLSprite.h
	g++ -Wall -g -c src/sdl2/SDLJeu.cpp -o obj/SDLJeu.o  

obj/SDLSprite.o: src/sdl2/SDLSprite.cpp src/sdl2/SDLSprite.h 
	g++ -Wall -g -c src/sdl2/SDLSprite.cpp -o obj/SDLSprite.o 

obj/Jeu.o: src/core/Jeu.cpp src/core/Jeu.h src/core/Joueur.h src/core/Monstre.h src/core/Map.h src/core/Competence.h
	g++ -Wall -g -c src/core/Jeu.cpp -o obj/Jeu.o

obj/Map.o: src/core/Map.cpp src/core/Map.h
	g++ -Wall -g -c src/core/Map.cpp -o obj/Map.o

obj/Joueur.o: src/core/Joueur.cpp src/core/Joueur.h src/core/Map.h
	g++ -Wall -g -c src/core/Joueur.cpp -o obj/Joueur.o

obj/Monstre.o: src/core/Monstre.cpp src/core/Monstre.h src/core/Joueur.h src/core/Map.h
	g++ -Wall -g -c src/core/Monstre.cpp -o obj/Monstre.o

obj/TirMystique.o : src/core/Competences/TirMystique.cpp src/core/Competences/TirMystique.h src/core/Competence.h
	g++ -Wall -g -c src/core/Competences/TirMystique.cpp -o obj/TirMystique.o

obj/Flash.o : src/core/Competences/Flash.cpp src/core/Competences/Flash.h src/core/Competence.h
	g++ -Wall -g -c src/core/Competences/Flash.cpp -o obj/Flash.o

obj/Competence.o : src/core/Competence.cpp src/core/Competence.h src/core/Joueur.h src/core/Map.h
	g++ -Wall -g -c src/core/Competence.cpp -o obj/Competence.o

obj/CalculTrajectoire.o : src/extern/CalculTrajectoire.cpp src/extern/CalculTrajectoire.h src/core/Map.h  src/core/Monstre.h
	g++ -Wall -g -c src/extern/CalculTrajectoire.cpp -o obj/CalculTrajectoire.o

doc: doc/doxyfile
	doxygen doc/doxyfile

clean:
	rm -rf bin/*
	rm -rf obj/*